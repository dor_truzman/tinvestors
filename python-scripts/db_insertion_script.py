from pymongo import MongoClient
from datetime import datetime
import random
from faker import Faker
from utils import *
import openpyxl
import os
from bson import ObjectId



client = MongoClient("mongodb+srv://mongodb:mongodb@cluster0.od5ccku.mongodb.net/Tinvestors")
db = client["Tinvestors"]
collection = db["users"]
project_ids = [str(project["_id"]) for project in collection.find({}, {"_id": 1})]
# investors = [str(user["_id"]) for user in collection.find({"user_type": "ENTREPRENEUR"}, {"_id": 1})]
print(project_ids)

counter = 0


def insert_message(message, chat_room_id, from_user_id, to_user_id):
    # Connect to the MongoDB server
    global counter
    collection = db["messages"]

    # Create a new document with the message details
    document = {
        "message": message,
        "chat_room_name": chat_room_id,
        "from_user_id": from_user_id,
        "to_user_id": to_user_id,
        "time": datetime.now()
    }

    # Insert the document into the collection
    result = collection.insert_one(document)

    # Print the inserted document ID
    counter += 1
    print(counter, " Inserted document ID:", result.inserted_id)


def insert_fundings(amount, investors_ids, entrepreneur_id, project_id, is_group_funding):
    global counter
    collection = db["fundings"]

    document = {
        "amount": amount,
        "investors_ids": investors_ids,
        "entrepreneur_id": entrepreneur_id,
        "project_id": project_id,
        "is_group_fundings": is_group_funding
    }

    result = collection.insert_one(document)
    counter += 1
    print(counter, "Inserted document ID:", result.inserted_id)


def insert_project(title: str, description: str, requested_sum: int, entrepreneur_id: str, category: str,
                   images: list[str], liked_by: list[str]):
    global counter
    collection = db["projects"]
    document = {
        "title": title,
        "description": description,
        "requested_sum": requested_sum,
        "entrepreneur_id": entrepreneur_id,
        "category": category,
        "images": images,
        "liked_by": liked_by
    }

    result = collection.insert_one(document)
    counter = counter + 1
    print(counter, " Inserted document ID:", result.inserted_id)


def insert_user(email: str, password: str, name: int, age: int, photo_url: str, user_type: dict, experience: str,
                is_approved: bool, favorite_categories: list[str], relevant_links: list[str]):
    global counter
    collection = db["users"]
    document = {
        "email": email,
        "password": password,
        "name": name,
        "age": age,
        "photo_url": photo_url,
        "user_type": user_type,
        "experience": experience,
        "is_approved": is_approved,
        "favorite_categories": favorite_categories,
        "relevant_links": relevant_links
    }

    result = collection.insert_one(document)
    counter += 1
    print(counter, "Inserted document ID:", result.inserted_id)


def add_messages(amount_of_rooms: int):

    user_2 = ['6489edf7f183b3ff8849f73b', '6489edfaf183b3ff8849f73e', '6489edfaf183b3ff8849f741', '6489edfaf183b3ff8849f742', '6489edfaf183b3ff8849f744', '6489edfbf183b3ff8849f747', '6489edfdf183b3ff8849f752', '6489edfdf183b3ff8849f753', '6489edfef183b3ff8849f756', '6489edfef183b3ff8849f757']

    first_user_messages = ["hello", "how are you?",
                           "im fine, thanks",
                           "thats a nice project! i want to invest in it, can you tell me more about it?",
                           "thanks!"]
    second_user_messages = ["hey",
                            "im great, how are you?",
                            "* project description*",
                            "no problem!",
                            "see you soon!"]
    # for i in range(amount_of_rooms):
    #     chat_room_id = "room" + str(i)
    #     from_user_id = generate_random_user_id()
    #     to_user_id = generate_random_user_id()
    #     for i in range(5):
    #         insert_message(first_user_messages[i],
    #                        chat_room_id,
    #                        from_user_id,
    #                        to_user_id)
    #
    #         insert_message(first_user_messages[i],
    #                        chat_room_id,
    #                        to_user_id,
    #                        from_user_id)
    from_user_id = "648c4f8f741de145d53742ee"
    for i, user_id in enumerate(user_2):
        chat_room_id = "room" + str(i)
        to_user_id = user_2[i]
        for k in range(5):
            insert_message(first_user_messages[k],
                           chat_room_id,
                           ObjectId(from_user_id),
                           ObjectId(to_user_id))

            insert_message(first_user_messages[k],
                           chat_room_id,
                           ObjectId(to_user_id),
                           ObjectId(from_user_id))


def add_fundings(number_of_fundings):
    amount = random.randint(1, 100)
    investors_ids = generate_random_user_id_list(db["users"], "INVESTOR")
    entrepreneur_id = generate_random_user_id()
    project_id = generate_random_project_id()
    is_group_fundings = True

    for i in range(number_of_fundings):
        is_group_fundings = not is_group_fundings
        insert_fundings(amount, investors_ids, entrepreneur_id, project_id, is_group_fundings)


def add_project(number_of_projects):
    def generate_random_project_name():
        fake = Faker()
        project_name = fake.catch_phrase()
        return project_name

    def generate_random_description(project_name):
        fake = Faker()
        description = f"{project_name}: {fake.paragraph()}"
        return description

    for i in range(number_of_projects):
        title = generate_random_project_name()
        description = generate_random_description(title)
        requested_sum = random.randint(100000, 1000000)
        entrepreneur_id = generate_ent_id()
        category = category_list[random.randint(1, 14)]
        images = generate_random_image()
        liked_by = [generate_random_user_id() for _ in range(random.randint(1, 10))]
        insert_project(title, description, requested_sum, entrepreneur_id, category, images, liked_by)


def add_user(number_of_users):
    for i in range(number_of_users):
        email = generate_random_email(8)
        password = generate_random_number_string()
        name = generate_random_name()
        age = random.randint(18, 69)
        photo_url = 'https://www.shutterstock.com/image-vector/bearded-man-face-male-generic-260nw-2140768413.jpg'
        user_type = type_list[random.randint(1, 2) - 1]
        experience = generate_random_experience()
        is_approved = random.choice([True, False])
        favorite_categories = generate_random_favorite_categories()
        relevant_links = [generate_random_url(5, 'example') for _ in range(random.randint(1, 4))]
        insert_user(email, password, name, age, photo_url, user_type, experience, is_approved, favorite_categories,
                    relevant_links)


def write_to_excel(number_of_project_each):
    # Create a new Excel workbook and select the active sheet
    workbook = openpyxl.Workbook()
    sheet = workbook.active

    # Write headers to the first row
    sheet["A1"] = "userID"
    sheet["B1"] = "itemID"
    sheet["C1"] = "rating"

    # Generate random data for each investor and project
    for investor_id in investor_ids:
        for _ in range(number_of_project_each):
            project_id = random.choice(project_ids)
            random_number = random.randint(1, 5)

            # Get the current row number
            row_number = sheet.max_row + 1

            # Write the data to the Excel sheet
            sheet.cell(row=row_number, column=1, value=investor_id)
            sheet.cell(row=row_number, column=2, value=project_id)
            sheet.cell(row=row_number, column=3, value=random_number)

    # Save the Excel workbook
    desktop_path = os.path.join(os.path.expanduser("~"), "Desktop")

    # Save the Excel workbook on the desktop
    file_path = os.path.join(desktop_path, "investment_data.xlsx")
    workbook.save(file_path)
    print('excel saved in desktop')

def main():
    global counter
    # add_user(500)
    # # counter = 0
    # add_project(500)
    # # counter = 0
    add_messages(1)
    # counter = 0
    # add_fundings(1000)
    # write_to_excel(10)

if __name__ == "__main__":
    main()

