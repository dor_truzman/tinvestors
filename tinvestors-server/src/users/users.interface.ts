import { Document } from 'mongoose';

export enum userType {
  INVESTOR = 'INVESTOR',
  ENTREPRENEUR = 'ENTREPRENEUR',
}

export interface User extends Document {
  name: string;
  email: string;
  age: number;
  password: string;
  photo_url: string;
  user_type: {
    type: string;
    enum: ['INVESTOR', 'ENTREPRENEUR'];
    default: 'ENTREPRENEUR';
  };
  experience: string;
  is_approved: boolean;
  favorite_categories: [string];
  relevant_links: [string];
}
