/* eslint-disable @typescript-eslint/no-var-requires */
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { User } from './users.interface';
import { InjectModel } from '@nestjs/mongoose';
const sha256 = require('js-sha256');

@Injectable()
export class UsersService {
  constructor(@InjectModel('users') private userModel: Model<User>) {}

  public async create(createUserDto: Partial<User>): Promise<User> {
    createUserDto.password = sha256(createUserDto.password);
    const created = new this.userModel(createUserDto);
    return created.save();
  }

  public async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  public async findOne(id: string) {
    return this.userModel.findById(id).exec();
  }

  public async findOneByEmail(email: string) {
    return this.userModel.findOne({ email }).exec();
  }

  public async update(id: string, updateUserDto: Partial<User>) {
    return this.userModel.findByIdAndUpdate(id, updateUserDto).exec();
  }

  public async remove(id: string) {
    return this.userModel.findByIdAndRemove(id).exec();
  }

  public async verifyPassword(email: string, password: string): Promise<User> {
    const shaPassword = sha256(password);
    return this.userModel.findOne({ email, password: shaPassword });
  }
}
