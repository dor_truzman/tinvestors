import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  email: String,
  password: String,
  name: String,
  age: Number,
  photo_url: String,
  user_type: {
    type: String,
    enum: ['INVESTOR', 'ENTREPRENEUR'],
    default: 'ENTREPRENEUR',
  },
  experience: String,
  is_approved: Boolean,
  favorite_categories: [String],
  relevant_links: [String],
});
