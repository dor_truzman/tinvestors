import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StatisticsService } from './statistics.service';
import { StatisticsSchema } from './db/statistics.model';
import { StatisticsController } from './statistics.controller';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'statistics', schema: StatisticsSchema }])],
    controllers: [StatisticsController],
    providers: [StatisticsService],
})
export class StatisticsModule { }
