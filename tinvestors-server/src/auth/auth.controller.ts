import { Body, Controller, Get, Headers, Post, Request, SetMetadata } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from 'src/users/users.interface';

export const IS_PUBLIC_KEY = 'isPublic';
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @Post('signup')
  signup(@Body() signupRequest: Partial<User>) {
    return this.authService.signup(signupRequest);
  }

  @Public()
  @Post('signin')
  signin(@Body() signinRequest: { email: string; password: string }) {
    return this.authService.signIn(signinRequest.email, signinRequest.password);
  }

  @Get('verifyToken')
  verifyToken(@Request() req: any) {
    return this.authService.returnUserFromJWT(req);
  }
}
