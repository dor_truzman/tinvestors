import { Headers, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/users.interface';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
  constructor(private userService: UsersService, private jwtService: JwtService) {}

  async signup(signupRequest: Partial<User>) {
    let createdUser = await this.userService.create(signupRequest);
    return {
      user: createdUser,
      jwt: await this.createJWTForUser(createdUser),
    };
  }

  async signIn(email: string, password: string) {
    let user = await this.userService.verifyPassword(email, password);

    return {
      user: user,
      jwt: await this.createJWTForUser(user),
    };
  }

  async returnUserFromJWT(req: any) {
    return this.userService.findOneByEmail(req.user.sub);
  }

  private async createJWTForUser(user: User) {
    const payload = { sub: user.email, username: user.name };
    let token = await this.jwtService.signAsync(payload);
    return token;
  }
}
