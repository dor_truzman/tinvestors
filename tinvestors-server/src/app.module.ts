import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { MessagesModule } from './chat/messages.module';
import { FundingsModule } from './fundings/fundings.module';
import { StatisticsModule } from './statistics/statistics.module';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { databaseUrl } from './constants';
import { JwtModule } from '@nestjs/jwt';
import { ProjectModule } from './project/project.module';

@Module({
  imports: [
    AuthModule,
    MessagesModule,
    FundingsModule,
    StatisticsModule,
    UsersModule,
    ProjectModule,
    MongooseModule.forRoot(databaseUrl)],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule { }
