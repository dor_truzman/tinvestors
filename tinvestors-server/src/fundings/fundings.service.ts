import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Funding } from './fundings.interface';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class FundingsService {
  constructor(@InjectModel('fundings') private fundingModel: Model<Funding>) { }

  public async create(createFundingDto: Partial<Funding>): Promise<Funding> {
    const created = new this.fundingModel(createFundingDto);
    return created.save();
  }

  public async createGroupFunding(createFundingDto: Partial<Funding>): Promise<Funding> {
    createFundingDto.is_group_funding = true;
    const created = new this.fundingModel(createFundingDto);
    return created.save();
  }

  public async findAll(): Promise<Funding[]> {
    return this.fundingModel.find().populate(['investors_ids','entrepreneur_id','project_id']).exec();
  }

  public async findByAmount(minimumRequiredAmount: number): Promise<Funding[]> {
    return this.fundingModel.find({ requested_sum: { $lte: minimumRequiredAmount } }).populate(['investors_ids','entrepreneur_id','project_id']).exec();
  }

  public async findByCategory(category: string): Promise<Funding[]> {
    return this.fundingModel.find({ category }).populate(['investors_ids','entrepreneur_id','project_id']).exec();
  }

  public async findByLocation(location: string): Promise<Funding[] | void> {
    // TODO implement location in fundings
    // return this.fundingModel.find({ location }).exec();
  }

  public async findOne(id: string): Promise<Funding> {
    return this.fundingModel.findById(id).populate(['investors_ids','entrepreneur_id','project_id']).exec();
  }

  public async update(id: string, updateFundingDto: Partial<Funding>): Promise<Funding> {
    return this.fundingModel.findByIdAndUpdate(id, updateFundingDto, { new: true }).populate(['investors_ids','entrepreneur_id','project_id']).exec();
  }

  public async remove(id: string): Promise<Funding> {
    return this.fundingModel.findByIdAndRemove(id).populate(['investors_ids','entrepreneur_id','project_id']).exec();
  }
}
