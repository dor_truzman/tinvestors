import { Document } from 'mongoose';

export interface Funding extends Document {
  amount: number;
  investors_ids: string[];
  entrepreneur_id: string;
  project_id: string;
  is_group_funding: boolean;
}
