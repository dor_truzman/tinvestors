import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const FundingSchema = new mongoose.Schema({
  amount: Number,
  investors_ids: [{ type: Schema.Types.ObjectId, ref: 'users' }],
  entrepreneur_id: { type: Schema.Types.ObjectId, ref: 'users' },
  project_id: { type: Schema.Types.ObjectId, ref: 'projects' },
  is_group_funding: Boolean,
});
