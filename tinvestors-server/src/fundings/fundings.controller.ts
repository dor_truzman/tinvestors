import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { FundingsService } from './fundings.service';
import { Funding } from './fundings.interface';

@Controller('fundings')
export class FundingsController {
  constructor(private fundingsService: FundingsService) {}

  @Post()
  create(@Body() createFundingDto: Partial<Funding>) {
    return this.fundingsService.create(createFundingDto);
  }

  @Post('/groupFunding')
  createGroupFunding(@Body() createFundingDto: Partial<Funding>) {
    return this.fundingsService.createGroupFunding(createFundingDto);
  }

  @Get()
  findAll() {
    return this.fundingsService.findAll();
  }

  @Get('/byAmount')
  FindByAmount(@Param('minimumRequiredAmount') minimumRequiredAmount: number) {
    return this.fundingsService.findByAmount(minimumRequiredAmount);
  }

  @Get('byCategory')
  FindByCategory(@Param('category') category: string) {
    return this.fundingsService.findByCategory(category);
  }

  @Get('/byLocation')
  FindByLocation(@Param('location') location: string) {
    return this.fundingsService.findByLocation(location);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fundingsService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateFundingDto: Partial<Funding>) {
    return this.fundingsService.update(id, updateFundingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fundingsService.remove(id);
  }
}
