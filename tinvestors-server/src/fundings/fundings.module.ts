import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FundingsController } from './fundings.controller';
import { FundingsService } from './fundings.service';
import { FundingSchema } from './db/fundings.model';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'fundings', schema: FundingSchema }])],
  controllers: [FundingsController],
  providers: [FundingsService],
})
export class FundingsModule { }
