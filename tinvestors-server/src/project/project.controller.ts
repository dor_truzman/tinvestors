import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { LikeProjectRequestDto } from './dto/like.project.dto';
import { Project } from './project.interface';
import { ProjectService } from './project.service';

@Controller('projects')
export class projectController {
  constructor(private projectService: ProjectService) {}

  @Post()
  create(@Body() createProjectDto: Partial<Project>) {
    return this.projectService.create(createProjectDto);
  }

  @Get()
  findAll() {
    return this.projectService.findAll();
  }

  @Get('/byAmount')
  findByAmount(@Param('minimumRequiredAmount') minimumRequiredAmount: number) {
    return this.projectService.findByAmount(minimumRequiredAmount);
  }

  @Post('like')
  likeProject(@Body() likeProjectRequestDto: LikeProjectRequestDto) {
    return this.projectService.likeProject(likeProjectRequestDto);
  }

  @Get('byCategory/:category')
  findByCategory(@Param('category') category: string) {
    return this.projectService.findByCategory(category);
  }

  @Get('byUser/:id')
  findByEntrepreneur(@Param('id') id: string) {
    return this.projectService.findByEntrepreneur(id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.projectService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateProjectDto: Partial<Project>) {
    return this.projectService.update(id, updateProjectDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectService.remove(id);
  }
}
