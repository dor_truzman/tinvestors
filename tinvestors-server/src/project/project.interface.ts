import { Document } from 'mongoose';

export interface Project extends Document {
  title: string;
  description: string;
  requested_sum: number;
  entrepreneur_id: string;
  categories: string[];
  images: string[];
  liked_by: string[];
}
