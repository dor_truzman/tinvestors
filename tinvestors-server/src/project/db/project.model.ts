import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

enum category {
  "Health and Wellness" = "Health and Wellness",
  "E-commerce and Retail" = "E-commerce and Retail",
  "EdTech" = "EdTech",
  "FinTech" = "FinTech",
  "Sustainability and GreenTech" = "Sustainability and GreenTech",
  "AI and Machine Learning" = "AI and Machine Learning",
  "SaaS (Software as a Service)" = "SaaS (Software as a Service)",
  "IoT (Internet of Things)" = "IoT (Internet of Things)",
  "Travel and Tourism" = "Travel and Tourism",
  "Food and Beverage" = "Food and Beverage",
  "Entertainment and Media" = "Entertainment and Media",
  "Social Impact and Non-Profit" = "Social Impact and Non-Profit",
  "Cybersecurity" = "Cybersecurity",
  "Real Estate and Property Tech" = "Real Estate and Property Tech",
  "Automotive and Transportation" = "Automotive and Transportation"
}

export const ProjectSchema = new mongoose.Schema({
  title: String,
  description: String,
  requested_sum: Number,
  entrepreneur_id: { type: Schema.Types.ObjectId, ref: 'users' },
  category: { type: String, enum: category, default: null },
  images: [String],
  html: String,
  liked_by: [{ type: Schema.Types.ObjectId, ref: 'users' },],
});
