import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Project } from './project.interface';
import { LikeProjectRequestDto } from './dto/like.project.dto';
@Injectable()
export class ProjectService {
  constructor(@InjectModel('projects') private projectModel: Model<Project>) {}

  public async create(createProject: Partial<Project>): Promise<Project> {
    const created = new this.projectModel(createProject);
    return created.save();
  }

  public async findAll(): Promise<Project[]> {
    return this.projectModel.find().populate(['entrepreneur_id', 'liked_by']).limit(15).exec();
  }

  public async findByAmount(minimumRequiredAmount: number): Promise<Project[]> {
    return this.projectModel
      .find({ requested_sum: { $lte: minimumRequiredAmount } })
      .populate(['entrepreneur_id', 'liked_by'])
      .limit(100)
      .exec();
  }

  public async findByCategory(category: string): Promise<Project[]> {
    return this.projectModel.find({ category }).populate(['entrepreneur_id', 'liked_by']).limit(15).exec();
  }

  public async findByEntrepreneur(entrepreneur_id: string): Promise<Project[]> {
    return this.projectModel.find({ entrepreneur_id }).populate(['entrepreneur_id', 'liked_by']).limit(15).exec();
  }

  public async findOne(id: string): Promise<Project> {
    return this.projectModel.findById(id).populate(['entrepreneur_id', 'liked_by']).exec();
  }

  public async update(id: string, updateProjectDto: Partial<Project>): Promise<Project> {
    return this.projectModel
      .findByIdAndUpdate(id, updateProjectDto, { new: true })
      .populate(['entrepreneur_id', 'liked_by'])
      .exec();
  }

  public async remove(id: string): Promise<Project> {
    return this.projectModel.findByIdAndRemove(id).populate(['entrepreneur_id', 'liked_by']).exec();
  }

  public async likeProject(likeProjectRequestDto: LikeProjectRequestDto) {
    const project = await this.projectModel.findById(likeProjectRequestDto.projectId);
    if (!project) {
      return null;
    }

    const index = project.liked_by.indexOf(likeProjectRequestDto.investorId);

    if (index > -1) {
      project.liked_by.splice(index, 1);
    } else {
      project.liked_by.push(likeProjectRequestDto.investorId);
    }

    return this.projectModel
      .updateOne({ _id: likeProjectRequestDto.projectId }, { liked_by: project.liked_by })
      .populate(['entrepreneur_id', 'liked_by'])
      .exec();
  }
}
