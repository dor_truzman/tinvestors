import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProjectSchema } from './db/project.model';
import { projectController } from './project.controller';
import { ProjectService } from './project.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'projects', schema: ProjectSchema }])],
  controllers: [projectController],
  providers: [ProjectService],
})
export class ProjectModule { }
