import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const MessageSchema = new mongoose.Schema({
  message: String,
  chat_room_name: String,
  from_user_id: { type: Schema.Types.ObjectId, ref: 'users' },
  to_user_id: { type: Schema.Types.ObjectId, ref: 'users' },
  time: Date,
});