import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Message } from './messages.interface';
import { InjectModel } from '@nestjs/mongoose';
var mongoose = require('mongoose');

@Injectable()
export class MessagesService {

    constructor(@InjectModel('messages') private messageModel: Model<Message>) { }

    public async create(createMessageDto: Partial<Message>): Promise<Message> {
        const created = new this.messageModel(createMessageDto);
        return created.save();
    }

    public async findAllMessages() {
        return this.messageModel.find().populate(['from_user_id', 'to_user_id']).exec();
    }

    public async findMessagesByUserId(id: string) {
        var mongooseId = new mongoose.Types.ObjectId(id);
        return this.messageModel.find({ $or: [{ 'to_user_id': mongooseId }, { 'from_user_id': mongooseId }] }).populate(['from_user_id', 'to_user_id']).exec();
    }

    public async findMessagesByRoomId(name: string) {
        return this.messageModel.find({ 'chat_room_name': name }).populate(['from_user_id', 'to_user_id']).exec();
    }
    ש
    public async findRoomsByUserId(userId: string) {
        const result = await this.messageModel.find({ $or: [{ 'to_user_id': userId }, { 'from_user_id': userId }] }).populate(['from_user_id', 'to_user_id']).exec();
        const chatRooms = result.map((value) => value.chat_room_name)
        return [...new Set(chatRooms)]

    }
    public async addMessageToRoom(roomId: string, createMessageDto: Partial<Message>) {
        createMessageDto.chat_room_name = roomId
        const created = new this.messageModel(createMessageDto);
        return (await created.populate(['from_user_id', 'to_user_id'])).save();
    }

    public async update(id: string, updateMessageDto: Partial<Message>) {
        return this.messageModel.findByIdAndUpdate(id, updateMessageDto).populate(['from_user_id', 'to_user_id']).exec();
    }

    public async remove(id: string) {
        return this.messageModel.findByIdAndRemove(id).populate(['from_user_id', 'to_user_id']).exec();
    }

}
