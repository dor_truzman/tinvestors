import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { Message } from './messages.interface';

@Controller('messages')
export class MessagesController {
    constructor(private messagesService: MessagesService) { }

    @Post()
    create(@Body() createMessageDto: Partial<Message>) {
        return this.messagesService.create(createMessageDto);
    }

    @Get('user/:userId')
    findMessagesByUserId(@Param('userId') userId: string) {
        return this.messagesService.findMessagesByUserId(userId);
    }

    @Get('rooms/userid/:userId')
    findRoomsByUserId(@Param('userId') userId: string) {
        return this.messagesService.findRoomsByUserId(userId);
    }

    @Get('room/:roomName')
    findOne(@Param('roomName') chatRoomName: string) {
        return this.messagesService.findMessagesByRoomId(chatRoomName);
    }

    @Get('all')
    findAll() {
        return this.messagesService.findAllMessages();
    }
}
