import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from 'src/app.module';
import { Message } from '../messages.interface';

describe('Messages', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/POST create new message`, () => {
    let createDto: Partial<Message> = {
        message: "testingMsg",
        chat_room: "test_chat_room",
        from_user_id: "test_from_user_id",
        to_user_id: "test_to_user_id",
        time: new Date(Date.now())
    }

    return request(app.getHttpServer())
      .post(`/messages`)
      .send(createDto)
      .expect(200);
  });

  it(`/GET messages by user id`, () => {
    let MessageDto: Partial<Message>[] = [{
        message: "testingMsg",
        chat_room: "test_chat_room",
        from_user_id: "test_from_user_id",
        to_user_id: "test_to_user_id",
        time: new Date(Date.now())
    }]

    return request(app.getHttpServer())
      .get(`user/messages/test_to_user_id`)
      .expect(200)
      .expect({
        data: MessageDto // will have id then it should not work i belive
      });
  });

  it(`/GET messages by room id`, () => {
    let MessageDto2: Partial<Message>[] = [{
        message: "testingMsg",
        chat_room: "test_chat_room",
        from_user_id: "test_from_user_id",
        to_user_id: "test_to_user_id",
        time: new Date(Date.now())
    }]

    return request(app.getHttpServer())
      .get(`room/messages/test_chat_room`)
      .expect(200)
      .expect({
        data: MessageDto2
      });
  });

  afterAll(async () => {
    await app.close();
  });
});