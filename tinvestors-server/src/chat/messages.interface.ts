import { Document } from 'mongoose';

export interface Message extends Document {
  message: string,
  chat_room_name: string,
  from_user_id: string,
  to_user_id: string,
  time: Date,
}