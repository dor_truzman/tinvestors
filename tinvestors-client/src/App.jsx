import "./App.css";
import { Route, Routes } from "react-router-dom";
import Exposition from "./Pages/Exposition";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import Browser from "./Pages/Browser";
import Navbar from "./Components/Navbar";
import Profile from "./Pages/Profile";
import PostDisplay from "./Pages/PostDisplay";
import ProjectEditor from "./Pages/ProjectEditor";
import Chat from "./Pages/Chat";
import { UserContext } from "./Contexts/userContext";
import { useContext, useEffect, useState } from "react";
import { ENTRE_TYPE } from "./Constants/userTypes";
import { getEntity } from "./Services/fetchService";
import { CircularProgress } from "@mui/material";
import ProtectedRoute from "./ProtectedRoute";

function App() {
  const userState = useContext(UserContext);
  const [isLoadingUser, setIsLoadingUser] = useState(true);
  const [isLoginOpen, setIsLoginOpen] = useState(false);

  const updateConnectedUser = async () => {
    setIsLoadingUser(true);

    let foundUser;

    try {
      foundUser = await getEntity({
        name: "auth",
        route: "verifyToken",
      });
    } catch {}

    if (foundUser) {
      userState.setUser({ ...foundUser, password: undefined });
    }

    setIsLoadingUser(false);
  };

  useEffect(() => {
    updateConnectedUser();
  }, []);

  return (
    <>
      <Navbar isLoadingUser={isLoadingUser} setIsLoginOpen={setIsLoginOpen} />
      <div className="App">
        <Routes>
          <Route
            exact
            path="/"
            element={
              userState?.state?.user ? (
                userState.state.user.user_type === ENTRE_TYPE ? (
                  <Profile />
                ) : (
                  <Browser />
                )
              ) : !isLoadingUser ? (
                <Exposition />
              ) : (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyItems: "center",
                    alignItems: "center",
                    margin: 200,
                  }}
                >
                  <CircularProgress size={100} />
                </div>
              )
            }
          />
          <Route exact path="/register" element={<Register />} />
          <Route
            exact
            path="/browser"
            element={
              <ProtectedRoute>
                <Browser />
              </ProtectedRoute>
            }
          />
          <Route
            exact
            path="/profile"
            element={
              <ProtectedRoute>
                <Profile />
              </ProtectedRoute>
            }
          />
          <Route
            exact
            path="/post-display"
            element={
              <ProtectedRoute>
                <PostDisplay />
              </ProtectedRoute>
            }
          />
          <Route
            exact
            path="/project-editor"
            element={
              <ProtectedRoute>
                <ProjectEditor />
              </ProtectedRoute>
            }
          />
          <Route
            exact
            path="/chat"
            element={
              <ProtectedRoute>
                <Chat />
              </ProtectedRoute>
            }
          />
        </Routes>
        {isLoginOpen && (
          <Login isOpen={isLoginOpen} setIsOpen={setIsLoginOpen} />
        )}
      </div>
    </>
  );
}

export default App;
