const PROJECT_CATEGORIES = [
  "Health and Wellness",
  "E-commerce and Retail",
  "EdTech",
  "FinTech",
  "Sustainability and GreenTech",
  "AI and Machine Learning",
  "SaaS (Software as a Service)",
  "IoT (Internet of Things)",
  "Travel and Tourism",
  "Food and Beverage",
  "Entertainment and Media",
  "Social Impact and Non-Profit",
  "Cybersecurity",
  "Real Estate and Property Tech",
  "Automotive and Transportation",
];

export { PROJECT_CATEGORIES };
