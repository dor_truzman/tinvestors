import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import Lottie from "lottie-react";
import idAnimation from "../assets/58664-id-card-ui-animation.json";
import { useState } from "react";
import moment from "moment";

function getAge(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }

  return age;
}

function IdentificationForm({
  isOpen,
  setIsOpen,
  setIsValidated,
  validationDetails,
  setValidationDetails,
}) {
  const [dateOfBirth, setDateOfBirth] = useState(moment());
  const [socialLinks, setSocialLinks] = useState("");

  return (
    <Dialog fullWidth={true} open={isOpen} onClose={() => setIsOpen(false)}>
      <DialogTitle>Some extra details for verification...</DialogTitle>
      <DialogContent>
        <Box
          noValidate
          component="form"
          sx={{
            display: "flex",
            flexDirection: "column",
            m: "auto",
            textAlign: "center",
            width: "fit-content",
          }}
        >
          <DialogContentText>What's your date of birth?</DialogContentText>
          <DatePicker
            value={dateOfBirth}
            onChange={(newValue) => {
              setDateOfBirth(newValue);
              setValidationDetails({
                ...validationDetails,
                age: getAge(newValue),
              });
            }}
            sx={{ margin: 2 }}
          />

          <DialogContentText>
            Tell us a bit about your past experience.
          </DialogContentText>
          <TextField
            style={{ margin: 20 }}
            value={validationDetails.experience}
            onChange={(e) =>
              setValidationDetails({
                ...validationDetails,
                experience: e.target.value,
              })
            }
            multiline
            maxRows={2}
            minRows={2}
          />

          <DialogContentText>
            Provide social links (LinkedIn/Facebook/etc) separated by newlines.
          </DialogContentText>
          <TextField
            style={{ margin: 20 }}
            value={socialLinks}
            onChange={(e) => {
              setSocialLinks(e.target.value);
              setValidationDetails({
                ...validationDetails,
                relevant_links: e.target.value
                  .split("\n")
                  .filter((v) => v.length),
              });
            }}
            multiline
            maxRows={5}
            minRows={1}
          />

          <DialogContentText>
            Kindly upload a clear photo of your ID or driver's license.
          </DialogContentText>

          <Lottie
            animationData={idAnimation}
            loop
            style={{ height: 150, margin: 20 }}
          />
          <Button
            component="label"
            color={validationDetails.photo_url ? "primary" : "secondary"}
          >
            {validationDetails.photo_url ? "Saved ✓" : "Upload"}
            <input
              accept="image/*"
              type="file"
              hidden
              onChange={(e) => {
                if (e.target?.files?.length) {
                  setValidationDetails({
                    ...validationDetails,
                    photo_url: e.target.files[0],
                  });
                }
              }}
            />
          </Button>

          <Typography fontSize={18} fontWeight={"bold"}>
            Why do we require this?
          </Typography>
          <Typography fontSize={15}>
            Our team at Tinvestors wants to ensure you a safe and pleasant
            experience, and to make sure that the person you are communicating
            with is indeed a legitimate entrepreneur or investor. The image will
            be used for registration purposes only.
          </Typography>
          <Button
            variant="contained"
            disabled={
              !validationDetails.experience?.length ||
              !validationDetails.relevant_links?.length ||
              !validationDetails.photo_url
            }
            onClick={() => {
              setIsOpen(false);
              setIsValidated(true);
            }}
            style={{ marginTop: 20 }}
          >
            Submit
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

export default IdentificationForm;
