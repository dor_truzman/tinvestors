import React, { useContext, useEffect, useState } from "react";
import { TextInput } from "./TextInput.jsx";
import { MessageLeft, MessageRight } from "./Message.jsx";
import {CircularProgress, Paper, Typography} from "@mui/material";
import { UserContext } from "../Contexts/userContext.jsx";
import {getEntities} from "../Services/fetchService";

export default function ChatRoom({ currChat, currRoomId }) {
  const userState = useContext(UserContext);
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const userId =  userState?.state?.user?._id;

  // const fakeMessages = [
  //   {
  //     message: "Hi Orel",
  //     chat_room_id: "fakeChatRoomId",
  //     from_user_id: {
  //       _id: "1",
  //       name: "Dor Truzman",
  //       photoURL:
  //         "https://lh3.googleusercontent.com/a-/AOh14Gi4vkKYlfrbJ0QLJTg_DLjcYyyK7fYoWRpz2r4s=s96-c",
  //     },
  //     to_user_id: { _id: "2", name: "Orel Zluf" },
  //     time: new Date().getTime(),
  //   },
  //   {
  //     message: "Hi Dor",
  //     chat_room_id: "fakeChatRoomId",
  //     from_user_id: { _id: "2", name: "Orel Zluf" },
  //     to_user_id: { _id: "1", name: "Dor Truzman" },
  //     time: new Date().getTime(),
  //   },
  // ];

  const sendMessage = async (text) => {
    const messageObject = {
      message: text,
      chat_room_name: currRoomId,
      from_user_id: userState.state?.user,
      time: new Date().getTime(),
      to_user_id: currChat[0].to_user_id
    };

    // TODO: Submit message object to the API here
      setMessages([...messages, messageObject]);
  };

  useEffect(() => {
      if (userId) {
          console.log("currChat: ", currChat);
          setMessages(currChat);
      }
      else {
          console.log("no user id");
      }
  }, [userId, currRoomId]);

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        justifyItems: "center",
        alignContent: "center",
        alignSelf: "center",
        justifySelf: "center",
        width: "100%",
        marginLeft: "10%"
      }}
    >
      <Paper
        sx={{
          marginRight: "10%",
          width: "100%",
          height: "80vh",
          maxHeight: "700px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        <div
          style={{
            backgroundColor: "white",
            width: "100%",
            textAlign: "center",
          }}
        >
          <Typography fontSize={30} fontWeight={"bold"} color="black">
            CHAT ROOM
          </Typography>
        </div>
        <Paper
          id="style-1"
          sx={{
            width: "calc(100% - 20px)",
            margin: 3,
            overflowY: "scroll",
            height: "calc(100% - 80px)",
          }}
        >
        {loading ? (
                    <div style={{ margin: 200 }}>
                        <CircularProgress size={100} />
                    </div>
                ) :
          messages?.map((msgObj) => {
            if (msgObj.from_user_id?._id === userState.state?.user?._id) {
              return (
                <MessageRight
                  message={msgObj.message}
                  timestamp={msgObj.time}
                  photoURL={msgObj.to_user_id.photo_url}
                  displayName={msgObj.to_user_id.name}
                  avatarDisp={!!msgObj.to_user_id.photo_url}
                />
              );
            } else {
              return (
                <MessageLeft
                  message={msgObj.message}
                  timestamp={msgObj.time}
                  photoURL={msgObj.from_user_id.photo_url}
                  displayName={msgObj.from_user_id.name}
                  avatarDisp={!!msgObj.from_user_id.photo_url}
                />
              );
            }
          })}
        </Paper>
        <TextInput submit={sendMessage} />
      </Paper>
    </div>
  );
}
