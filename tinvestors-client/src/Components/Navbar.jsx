import { Button, IconButton } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import { useNavigate } from "react-router-dom";
import logo from "../assets/navlogo.png";
import { useContext } from "react";
import { UserContext } from "../Contexts/userContext";
import UserIcon from "@mui/icons-material/Person";
import ChatIcon from "@mui/icons-material/Chat";

function Navbar({ isLoadingUser, setIsLoginOpen }) {
  const navigate = useNavigate();
  const userState = useContext(UserContext);

  return (
    <div>
      <Box sx={{ flexGrow: 1, marginBottom: 10 }}>
        <AppBar position="fixed" color="primary">
          <Toolbar sx={{ justifyContent: "space-between" }}>
            <img
              alt="Logo"
              onClick={() => navigate("/")}
              src={logo}
              style={{
                height: 50,
                cursor: "pointer",
              }}
            />

            {userState?.state?.user ? (
              <div style={{ display: "flex", justifyContent: "space-evenly" }}>
                <>
                  <IconButton color="success" onClick={() => navigate("/chat")}>
                    <ChatIcon style={{ marginLeft: 5, marginRight: 5 }} />
                  </IconButton>
                </>
                <>
                  <Button color="primary" onClick={() => navigate("/profile")}>
                    <UserIcon style={{ marginLeft: 5, marginRight: 5 }} />
                    {userState?.state?.user.name +
                      " (" +
                      userState?.state?.user.user_type +
                      ")"}
                  </Button>
                </>

                <>
                  <Button
                    color="secondary"
                    onClick={() => {
                      localStorage.removeItem("token");
                      userState.unsetUser();
                      navigate("/");
                    }}
                  >
                    LOG OUT
                  </Button>
                </>
              </div>
            ) : (
              !isLoadingUser && (
                <div
                  style={{ display: "flex", justifyContent: "space-evenly" }}
                >
                  <>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => setIsLoginOpen(true)}
                    >
                      SIGN IN
                    </Button>
                  </>
                </div>
              )
            )}
          </Toolbar>
        </AppBar>
      </Box>
    </div>
  );
}

export default Navbar;
