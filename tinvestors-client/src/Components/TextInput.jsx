import React, { useState } from "react";
import SendIcon from "@mui/icons-material/Send";
import { Button, TextField } from "@mui/material";

export const TextInput = ({ submit }) => {
  const [text, setText] = useState("");
  return (
    <>
      <form
        style={{
          display: "flex",
          justifyContent: "center",
          width: "95%",
          margin: "auto",
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          value={text}
          onChange={(e) => setText(e.target.value)}
          label="Write your message here..."
          sx={{
            width: "100%",
          }}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => submit(text)}
        >
          <SendIcon />
        </Button>
      </form>
    </>
  );
};
