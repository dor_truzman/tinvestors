import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Drawer,
} from "@mui/material";
import { PROJECT_CATEGORIES } from "../Constants/projectCategories";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../Contexts/userContext";

function Sidebar({ isSidebarOpen, setIsSidebarOpen, setSelectedCategories }) {
  const userState = useContext(UserContext);
  const [localSelectedCategories, setLocalSelectedCategories] = useState(
    PROJECT_CATEGORIES.reduce((obj, item) => {
      return {
        ...obj,
        [item]: false,
      };
    }, {})
  );

  useEffect(() => {
    if (userState.state.user?.favorite_categories?.length) {
      const newObject = PROJECT_CATEGORIES.reduce((obj, item) => {
        return {
          ...obj,
          [item]: !!userState.state.user.favorite_categories.includes(item),
        };
      }, {});

      setLocalSelectedCategories(newObject);
      setSelectedCategories(newObject);
    }
  }, [userState.state]);

  return (
    <Drawer
      sx={{
        width: 380,
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          width: 380,
          boxSizing: "border-box",
        },
      }}
      onClose={() => setIsSidebarOpen(false)}
      anchor={"left"}
      open={isSidebarOpen}
    >
      <div
        style={{
          marginTop: 100,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          textAlign: "center",
        }}
      >
        <Button onClick={() => setIsSidebarOpen(false)} color="warning">
          CLOSE FILTERS
        </Button>
        <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
          <FormLabel component="legend">
            Select what you'd like to see:
          </FormLabel>
          <FormGroup>
            {PROJECT_CATEGORIES.map((category) => (
              <FormControlLabel
                key={"FormControlLabel-" + category}
                control={
                  <Checkbox
                    checked={localSelectedCategories[category]}
                    onChange={(e) => {
                      const newObject = {
                        ...localSelectedCategories,
                        [category]: e.target.checked,
                      };
                      setLocalSelectedCategories(newObject);
                      setSelectedCategories(newObject);
                    }}
                  />
                }
                label={category}
              />
            ))}
          </FormGroup>
        </FormControl>
      </div>
    </Drawer>
  );
}

export default Sidebar;
