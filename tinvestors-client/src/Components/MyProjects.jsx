import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import EditIcon from "@mui/icons-material/Edit";
import InfoIcon from "@mui/icons-material/Info";
import { useEffect } from "react";
import { useState } from "react";
import { getEntities } from "../Services/fetchService";
import { CircularProgress, IconButton, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "../Contexts/userContext";
import Lottie from "lottie-react";
import notFoundAnimation from "../assets/14171-empty.json";

function MyProjects({ userId }) {
  const userState = useContext(UserContext);
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const fetchProjects = async () => {
    try {
      setLoading(true);
      setProjects(
        await getEntities({
          name: `projects/byUser/${userId}`,
        })
      );
      setLoading(false);
    } catch (e) {
      console.log("Error fetching projects: ", e);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (userId) {
      fetchProjects();
    }
  }, [userId]);

  return (
    <List sx={{ width: "100%" }}>
      {loading ? (
        <div style={{ margin: 200 }}>
          <CircularProgress size={100} />
        </div>
      ) : projects?.length ? (
        projects.map((value) => {
          const labelId = `list-secondary-label-${value._id}`;
          return (
            <ListItem
              key={`MyProjects-${value._id}`}
              secondaryAction={
                <>
                  {userId === userState?.state?.user?._id ? (
                    <IconButton
                      onClick={() =>
                        navigate("/project-editor", {
                          state: {
                            projectToEdit: {
                              _id: value._id,
                              title: value.title,
                              description: value.description,
                              requested_sum: value.requested_sum,
                              images: value.images,
                              category: value.category,
                              html: value.html,
                            },
                          },
                        })
                      }
                    >
                      <EditIcon />
                    </IconButton>
                  ) : (
                    <IconButton
                      onClick={() =>
                        navigate("/post-display", {
                          state: {
                            ...value,
                          },
                        })
                      }
                    >
                      <InfoIcon />
                    </IconButton>
                  )}
                </>
              }
            >
              <ListItemAvatar>
                <Avatar alt={`Avatar ${value + 1}`} src={value.images?.[0]} />
              </ListItemAvatar>
              <ListItemText id={labelId} primary={value.title} />
            </ListItem>
          );
        })
      ) : (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              justifyItems: "center",
              alignContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <Typography fontStyle="italic" color="white">
              Nothing to see here...
            </Typography>
            <Lottie
              animationData={notFoundAnimation}
              loop
              style={{ height: 200 }}
            />
          </div>
        </>
      )}
    </List>
  );
}

export default MyProjects;
