import { Avatar } from "@mui/material";
import { deepOrange } from "@mui/material/colors";
import moment from "moment";

export const MessageLeft = (props) => {
  const message = props.message ? props.message : "";
  const timestamp = props.timestamp ? props.timestamp : "";
  const photoURL = props.photoURL ? props.photoURL : "dummy.js";
  const displayName = props.displayName ? props.displayName : "";
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          fontFamily: "Myriad Pro Regular",
        }}
      >
        <Avatar
          alt={displayName}
          sx={{
            color: "black",
            backgroundColor: deepOrange[500],
            marginRight: 1,
          }}
          src={photoURL}
        />
        <div>
          <div
            style={{
              displayName: { color: "black", marginLeft: "20px" },
            }}
          >
            {displayName}
          </div>
          <div
            style={{
              color: "black",
              position: "relative",
              marginLeft: "20px",
              marginBottom: "10px",
              padding: "10px",
              backgroundColor: "lightgray",
              width: "60%",
              textAlign: "left",
              border: "1px solid #97C6E3",
              borderRadius: "10px",
              "&:after": {
                content: "''",
                position: "absolute",
                width: "0",
                height: "0",
                borderTop: "15px solid #A8DDFD",
                borderLeft: "15px solid transparent",
                borderRight: "15px solid transparent",
                top: "0",
                left: "-15px",
              },
              "&:before": {
                content: "''",
                position: "absolute",
                width: "0",
                height: "0",
                borderTop: "17px solid #97C6E3",
                borderLeft: "16px solid transparent",
                borderRight: "16px solid transparent",
                top: "-1px",
                left: "-17px",
              },
            }}
          >
            <div>
              <p
                style={{
                  minWidth: 180,
                  marginRight: 0,
                  marginLeft: 0,
                  marginTop: 3,
                  marginBottom: 3,
                  color: "black",
                }}
              >
                {message}
              </p>
            </div>
            <div
              style={{
                position: "absolute",
                fontSize: ".85em",
                fontWeight: "300",
                marginTop: "10px",
                bottom: "-3px",
                right: "5px",
              }}
            >
              {moment(timestamp).format("MM/DD/YY hh:mm")}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export const MessageRight = (props) => {
  const message = props.message ? props.message : "no message";
  const timestamp = props.timestamp ? props.timestamp : "";
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "flex-end",
        fontFamily: "Myriad Pro Regular",
      }}
    >
      <div
        style={{
          color: "black",
          position: "relative",
          marginRight: "20px",
          marginBottom: "10px",
          padding: "10px",
          backgroundColor: "rgb(96 188 243)",
          width: "60%",
          textAlign: "left",
          border: "1px solid #dfd087",
          borderRadius: "10px",
          "&:after": {
            content: "''",
            position: "absolute",
            width: "0",
            height: "0",
            borderTop: "15px solid #f8e896",
            borderLeft: "15px solid transparent",
            borderRight: "15px solid transparent",
            top: "0",
            right: "-15px",
          },
          "&:before": {
            content: "''",
            position: "absolute",
            width: "0",
            height: "0",
            borderTop: "17px solid #dfd087",
            borderLeft: "16px solid transparent",
            borderRight: "16px solid transparent",
            top: "-1px",
            right: "-17px",
          },
        }}
      >
        <p
          style={{
            minWidth: 180,
            padding: 0,
            marginRight: 0,
            marginLeft: 0,
            marginTop: 3,
            marginBottom: 3,
            color: "black",
          }}
        >
          {message}
        </p>
        <div
          style={{
            position: "absolute",
            fontSize: ".85em",
            fontWeight: "300",
            marginTop: "10px",
            bottom: "-3px",
            right: "5px",
          }}
        >
          {moment(timestamp).format("MM/DD/YY hh:mm")}
        </div>
      </div>
    </div>
  );
};
