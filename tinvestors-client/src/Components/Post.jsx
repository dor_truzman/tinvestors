import ThumbUp from "@mui/icons-material/ThumbUpAlt";
import ThumbUpOff from "@mui/icons-material/ThumbUpOffAlt";
import Chat from "@mui/icons-material/ChatOutlined";
import Enter from "@mui/icons-material/InfoOutlined";
import {
  Box,
  Card,
  Divider,
  Fab,
  Grid,
  IconButton,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import Placeholder from "../assets/Placeholder.png";
import { postEntity } from "../Services/fetchService";
import { useNavigate } from "react-router-dom";
import { VisibilityContext } from "react-horizontal-scrolling-menu";
import { useContext } from "react";

function Post({
  _id,
  title,
  description,
  entrepreneur_id,
  liked_by,
  current_username,
  requested_sum,
  images,
  category,
  html,
  itemId,
  setFinishedWatching,
}) {
  const [likeStatus, setLike] = useState(false);
  const [matchPercent, setMatchPercent] = useState(null);
  const navigate = useNavigate();
  const visibility = useContext(VisibilityContext);
  const visible = visibility.isItemVisible(itemId);
  const [visiblityTime, setVisiblityTime] = useState();

  useEffect(() => {
    setLike(
      !!(
        liked_by.length &&
        liked_by.find((user) => user._id === current_username)
      )
    );
  }, [_id]);

  const goToPostDisplay = () => {
    navigate("/post-display", {
      state: {
        _id,
        title,
        description,
        entrepreneur_id,
        liked_by: likeStatus
          ? [...liked_by, { _id: current_username }]
          : liked_by,
        current_username,
        requested_sum,
        images,
        category,
        matchPercent,
        html,
      },
    });
  };

  const calcMatch = async () => {
    try {
      setMatchPercent(
        await postEntity({
          name: "predict_rating",
          entity: {
            user_id: current_username,
            item_id: _id,
          },
          useAi: true,
          useFormData: true,
        })
      );
    } catch (e) {
      setMatchPercent(null);
    }
  };

  useEffect(() => {
    if (!visiblityTime && visible) {
      if (matchPercent === null) calcMatch();
      setVisiblityTime(new Date().getTime());
    } else if (!visible && visiblityTime) {
      setFinishedWatching({
        postId: _id,
        userId: current_username,
        timeWatched: new Date().getTime() - visiblityTime,
      });
      setVisiblityTime(null);
    }
  }, [visible]);

  const setLikeStatus = async (newStatus) => {
    await postEntity({
      name: "projects",
      entity: {
        projectId: _id,
        investorId: current_username,
      },
      route: "like",
    });

    setLike(newStatus);
  };

  return (
    <Card
      raised
      sx={{
        width: 330,
        mx: 2,
        my: 2,
        outlineColor: "primary.main",
        "&:hover": {
          cursor: "pointer",
          outline: 3,
          outlineStyle: "solid",
        },
        borderRadius: 5,
      }}
    >
      <Box
        sx={{
          my: 3,
          mx: 2,
          height: 80,
        }}
        onClick={() => goToPostDisplay()}
      >
        <Grid container alignItems="center" style={{ height: 70 }}>
          <Grid item xs>
            <Typography
              gutterBottom
              fontSize={20}
              style={{ fontWeight: "bold" }}
              component="div"
            >
              {title.length > 60 ? title.substring(0, 60) + "..." : title}
            </Typography>
          </Grid>
        </Grid>
        <Grid item style={{ height: 10 }}>
          <Typography gutterBottom fontSize={15} component="div">
            BY {entrepreneur_id?.name?.toUpperCase() || ""}
          </Typography>
        </Grid>
      </Box>
      <Divider variant="middle" />
      <Box
        sx={{ mx: 2 }}
        display="flex"
        alignItems="center"
        justifyContent="center"
        flexDirection={"column"}
        height={100}
        onClick={() => goToPostDisplay()}
      >
        <Typography gutterBottom component="div">
          {description.length > 100
            ? description.substring(0, 100) + "..."
            : description}
        </Typography>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        flexDirection={"column"}
        height={100}
        marginBottom={2}
        onClick={() => goToPostDisplay()}
      >
        <img
          src={images?.length ? images[0] : Placeholder}
          style={{
            maxWidth: 100,
            maxHeight: 100,
            margin: 10,
            borderRadius: "20%",
          }}
          alt="Post"
        />
      </Box>
      <Box display="flex" justifyContent="space-around" alignItems="center">
        <Tooltip title="Show project info">
          <IconButton
            size="large"
            color="secondary"
            aria-label="upload picture"
            component="label"
            onClick={() =>
              navigate("/post-display", {
                state: {
                  _id,
                  title,
                  description,
                  entrepreneur_id,
                  liked_by: likeStatus
                    ? [...liked_by, { _id: current_username }]
                    : liked_by,
                  current_username,
                  requested_sum,
                  images,
                  category,
                  matchPercent,
                  html,
                },
              })
            }
          >
            <Enter style={{ fontSize: 30 }} />
          </IconButton>
        </Tooltip>
        <Tooltip title={(likeStatus ? "Unlike" : "Like") + " this project"}>
          <IconButton
            size="large"
            color="primary"
            aria-label="upload picture"
            component="label"
            onClick={() => setLikeStatus(!likeStatus)}
          >
            {likeStatus ? (
              <ThumbUp style={{ fontSize: 30 }} />
            ) : (
              <ThumbUpOff style={{ fontSize: 30 }} />
            )}
          </IconButton>
        </Tooltip>
        <Tooltip title="Chat with the entrepreneur">
          <IconButton
            size="large"
            color="success"
            aria-label="upload picture"
            component="label"
            onClick={() =>
              navigate("/chat", {
                state: {
                  _id,
                  title,
                  description,
                  entrepreneur_id,
                  liked_by: likeStatus
                    ? [...liked_by, { _id: current_username }]
                    : liked_by,
                  current_username,
                  requested_sum,
                  images,
                  category,
                  matchPercent,
                  html,
                },
              })
            }
          >
            <Chat style={{ fontSize: 30 }} />
          </IconButton>
        </Tooltip>
        <Tooltip title="Match percentage">
          <Fab
            style={{ cursor: "default" }}
            variant="extended"
            size="small"
            color={
              matchPercent <= 50
                ? "secondary"
                : matchPercent < 80
                ? "primary"
                : "success"
            }
          >
            <Typography fontWeight={"bold"} fontSize={12}>
              {matchPercent ? matchPercent : "...."}% MATCH
            </Typography>
          </Fab>
        </Tooltip>
      </Box>
    </Card>
  );
}

export default Post;
