import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  Input,
  ListItemText,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Typography,
} from "@mui/material";
import { useState } from "react";
import * as isEmail from "is-email";
import VerifiedUser from "@mui/icons-material/VerifiedUserSharp";
import { ENTRE_TYPE, INVESTOR_TYPE } from "../Constants/userTypes";
import { PROJECT_CATEGORIES } from "../Constants/projectCategories";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function RegistrationForm({
  setIsOpen,
  isValidated,
  register,
  validationDetails,
}) {
  const [registrationDetails, setRegistrationDetails] = useState({
    favorite_categories: [PROJECT_CATEGORIES[0]],
  });

  return (
    <FormControl style={{ width: "50%" }}>
      <Input
        style={{ fontSize: 20, marginBottom: 10 }}
        placeholder="What's your name?"
        id="name"
        value={registrationDetails.name}
        onChange={(e) =>
          setRegistrationDetails({
            ...registrationDetails,
            name: e.target.value,
          })
        }
      />
      <FormControl style={{ flexDirection: "row" }}>
        <FormLabel style={{ padding: 15, fontWeight: "bold" }}>
          What are you?
        </FormLabel>
        <RadioGroup
          row
          defaultValue={INVESTOR_TYPE}
          onChange={(e) =>
            setRegistrationDetails({
              ...registrationDetails,
              user_type: e.target.value,
            })
          }
        >
          <FormControlLabel
            value={INVESTOR_TYPE}
            control={<Radio />}
            label="Investor"
          />
          <FormControlLabel
            value={ENTRE_TYPE}
            control={<Radio />}
            label="Entrepreneur"
          />
        </RadioGroup>
      </FormControl>
      <FormControl sx={{ flexDirection: "row" }}>
        <FormLabel style={{ padding: 15, fontWeight: "bold", marginTop: 20 }}>
          What do you like?
        </FormLabel>
        <Select
          sx={{ m: 1, width: 300 }}
          multiple
          value={registrationDetails.favorite_categories}
          onChange={(e) =>
            setRegistrationDetails({
              ...registrationDetails,
              favorite_categories: e.target.value,
            })
          }
          renderValue={(v) => v.join(", ")}
          style={{ marginTop: 20 }}
          MenuProps={MenuProps}
        >
          {PROJECT_CATEGORIES &&
            PROJECT_CATEGORIES.map((type, idx) => (
              <MenuItem value={type} key={`favCategoryMenuItem${idx}`}>
                <Checkbox
                  checked={registrationDetails.favorite_categories?.includes(
                    type
                  )}
                />
                <ListItemText primary={type} />
              </MenuItem>
            ))}
        </Select>
      </FormControl>
      <Input
        style={{ fontSize: 20 }}
        placeholder="Your email address"
        id="email"
        value={registrationDetails.email}
        onChange={(e) =>
          setRegistrationDetails({
            ...registrationDetails,
            email: e.target.value,
          })
        }
      />
      <Input
        style={{ fontSize: 20 }}
        placeholder="Your password"
        id="password"
        type="password"
        value={registrationDetails.password}
        onChange={(e) =>
          setRegistrationDetails({
            ...registrationDetails,
            password: e.target.value,
          })
        }
      />

      {isValidated ? (
        <div style={{ display: "flex", justifyContent: "center", margin: 20 }}>
          <Typography
            fontWeight={"bold"}
            style={{ marginRight: 5, fontSize: 15 }}
          >
            VERIFICATION DETAILS SAVED
          </Typography>
          <VerifiedUser />
        </div>
      ) : (
        <Button
          style={{ margin: 20, fontSize: 15 }}
          disabled={isValidated}
          color={isValidated ? "success" : "primary"}
          onClick={() => setIsOpen(true)}
        >
          Verification Details
        </Button>
      )}
      <Button
        onClick={() => {
          register({
            ...registrationDetails,
            ...validationDetails,
          });
        }}
        disabled={
          !isValidated ||
          !registrationDetails.name?.length ||
          !registrationDetails.password?.length ||
          !registrationDetails.email?.length ||
          !isEmail(registrationDetails.email)
        }
        style={{ margin: 20, fontSize: 20, fontWeight: "bold" }}
        variant="contained"
        color="primary"
      >
        Sign Up
      </Button>
    </FormControl>
  );
}

export default RegistrationForm;
