import { Box, Typography } from "@mui/material";
import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../Contexts/userContext";
import "./ProfileCard.css";

function Profile({
  name,
  profileType,
  experience,
  favorite_categories,
  relevant_links,
}) {
  const image =
    "https://w7.pngwing.com/pngs/247/564/png-transparent-computer-icons-user-profile-user-avatar-blue-heroes-electric-blue-thumbnail.png";
  // const classes = useStyles();
  const navigate = useNavigate();
  const userState = useContext(UserContext);
  const userInfo = userState?.state?.user || {};

  return (
    <>
      <figure className="snip1336">
        <img src="https://picsum.photos/440/320" alt="sample69" />
        <figcaption>
          <img
            src={userInfo?.photo_url}
            alt="profile-sample5"
            className="profile"
          />
          <h2>
            {userInfo?.name}
            <span>{userInfo?.user_type}</span>
          </h2>
          <Typography variant="h5">Interests</Typography>
          <Typography gutterBottom component="div">
            {userInfo?.favorite_categories?.join(", ")}
          </Typography>
          <Typography gutterBottom component="div">
            {userInfo?.experience}
          </Typography>
          <Box
            justifyContent="center"
            alignItems="center"
            display={"flex"}
            flexDirection={"row"}
          >
            {userInfo?.relevant_links?.map((link) => (
              <div>
                <a href={link} className="link_button">
                  {link.toString().split(".")[1]}
                </a>
              </div>
            ))}
          </Box>
        </figcaption>
      </figure>
    </>
  );
}

export default Profile;
