import React, {useContext, useEffect, useState} from "react";
import {UserContext} from "../Contexts/userContext";
import {CircularProgress, Paper, Typography} from "@mui/material";
import {MessageLeft, MessageRight} from "./Message";
import {TextInput} from "./TextInput";
import "./ChatRoomList.css";
import {getEntities} from "../Services/fetchService";
import Lottie from "lottie-react";
import notFoundAnimation from "../assets/14171-empty.json";

export default function ChatRoomList({ chatList, loading, setCurrRoomId }) {
    const userState = useContext(UserContext);

    {/*const fakeChats = [*/}
    {/*    {*/}
    {/*        last_message: "Hi Orel",*/}
    {/*        chat_room_id: "fakeChatRoomId",*/}
    {/*        from_user_id: {*/}
    {/*            _id: "1",*/}
    {/*            name: "Dor Truzman",*/}
    //             photoURL:
    //                 "https://lh3.googleusercontent.com/a-/AOh14Gi4vkKYlfrbJ0QLJTg_DLjcYyyK7fYoWRpz2r4s=s96-c",
    //         },
    //         to_user_id: { _id: "2", name: "Orel Zluf" },
    {/*        time: new Date(),*/}
    {/*    },*/}
    {/*    {*/}
    {/*        last_message: "Hi Dor",*/}
    {/*        chat_room_id: "fakeChatRoomId",*/}
    {/*        from_user_id: { _id: "2", name: "Orel Zluf" },*/}
    {/*        to_user_id: { _id: "1", name: "Dor Truzman" },*/}
    {/*        time: new Date(),*/}
    //     },
    //     {
    //         last_message: "Hi Orel",
    //         chat_room_id: "fakeChatRoomId",
    {/*        from_user_id: {*/}
    {/*            _id: "1",*/}
    {/*            name: "Dor Truzman",*/}
    {/*            photoURL:*/}
    {/*                "https://lh3.googleusercontent.com/a-/AOh14Gi4vkKYlfrbJ0QLJTg_DLjcYyyK7fYoWRpz2r4s=s96-c",*/}
    {/*        },*/}
    {/*        to_user_id: { _id: "2", name: "Orel Zluf" },*/}
    //         time: new Date(),
    //     },
    //     {
    {/*        last_message: "Hi Orel",*/}
    {/*        chat_room_id: "fakeChatRoomId",*/}
    {/*        from_user_id: {*/}
    {/*            _id: "1",*/}
    {/*            name: "Dor Truzman",*/}
    {/*            photoURL:*/}
    {/*                "https://lh3.googleusercontent.com/a-/AOh14Gi4vkKYlfrbJ0QLJTg_DLjcYyyK7fYoWRpz2r4s=s96-c",*/}
    {/*        },*/}
    {/*        to_user_id: { _id: "2", name: "Orel Zluf" },*/}
    //         time: new Date(),
    //     },
    //     {
    {/*        last_message: "Hi Orel",*/}
    //         chat_room_id: "fakeChatRoomId",
    //         from_user_id: {
    //             _id: "1",
    //             name: "Dor Truzman",
    //             photoURL:
    //                 "https://lh3.googleusercontent.com/a-/AOh14Gi4vkKYlfrbJ0QLJTg_DLjcYyyK7fYoWRpz2r4s=s96-c",
    //         },
    //         to_user_id: { _id: "2", name: "Orel Zluf" },
    //         time: new Date(),
    //     },
    // ];

    return (
        <div className="content container-fluid">
                    <div className="chat-window">
                        <div className="chat-cont-left">
                            <div className="chat-header">
                                <Typography variant="h5"
                                            color={"white"}
                                            style={{textAlign: "center"}}>
                                    Chats
                                </Typography>
                            </div>
                            {/*<form className="chat-search">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <div className="input-group-prepend">*/}
                            {/*            <i className="fas fa-search"></i>*/}
                            {/*        </div>*/}
                            {/*        <input type="text" className="form-control" placeholder="Search"/>*/}
                            {/*    </div>*/}
                            {/*</form>*/}
                            <div className="chat-users-list">
                                <div className="chat-scroll">
                                    { loading ? (
                                            <div style={{ margin: 200 }}>
                                                <CircularProgress size={100} />
                                            </div>
                                        ) : chatList?.length == 0 ? (
                                        <div
                                            style={{
                                                minHeight: "100%",
                                                textAlign: "center",
                                                justifyContent: "center",
                                                justifyItems: "center",
                                                alignContent: "center",
                                                alignItems: "center",
                                            }}
                                        >
                                            <Typography fontStyle="italic" color="black">
                                                Nothing to see here...
                                            </Typography>
                                            <Lottie
                                                animationData={notFoundAnimation}
                                                loop
                                            />
                                        </div>
                                        ) : (
                                            chatList?.map((currChat) => {
                                                return (
                                                    <a href="javascript:void(0);" className="media" style={{fontFamily: "Myriad Pro Regular"}} onClick={() => setCurrRoomId(currChat[0].chat_room_name)}>
                                                        <div className="media-img-wrap">
                                                            <div className="avatar avatar-away">
                                                                <img
                                                                    src={currChat?.at(0)?.to_user_id?.photo_url}
                                                                    alt="User Image" className="avatar-img rounded-circle"/>
                                                            </div>
                                                        </div>
                                                        <div className="media-body">
                                                            <div>
                                                                <div className="user-name">{currChat?.at(0).to_user_id.name}</div>
                                                                <div className="user-last-chat">{currChat?.at(currChat.at(0).length - 1)?.message}</div>
                                                            </div>
                                                            <div>
                                                                <div className="last-chat-time block">{currChat?.at(0).time}</div>
                                                                <div className="badge badge-success badge-pill">1</div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                );
                                            })
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    );
}