import React, { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "./Contexts/userContext";

const ProtectedRoute = ({ children }) => {
  const userState = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!userState?.state?.user) return navigate("/");
  }, [userState.state]);

  return <>{userState?.state?.user ? children : null}</>;
};

export default ProtectedRoute;
