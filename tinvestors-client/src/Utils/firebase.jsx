import { initializeApp } from "firebase/app";
import {
  getDownloadURL,
  getStorage,
  ref,
  uploadBytesResumable,
} from "firebase/storage";

// Initialize Firebase
const app = initializeApp({
  apiKey: "AIzaSyBb7OvchpwTbQNClr1GaqTk6Y0aKf9Yw3o",
  authDomain: "t-skyline-388700.firebaseapp.com",
  projectId: "t-skyline-388700",
  storageBucket: "t-skyline-388700.appspot.com",
  messagingSenderId: "830052567992",
  appId: "1:830052567992:web:5299a470c9a39b56989d4d",
});

// Firebase storage reference
const storage = getStorage(app);

const uploadAndGetURL = (file) => {
  return new Promise((resolve, reject) => {
    const storageRef = ref(storage, `/files/${file.name}`);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      null,
      (err) => reject(err),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          resolve(url);
        });
      }
    );
  });
};

export { storage, uploadAndGetURL };
