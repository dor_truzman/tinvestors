import React, { useState } from "react";

export const UserContext = React.createContext({
  state: {},
  setUser: () => {},
  unsetUser: () => {},
});

export const UserContextProvider = (props) => {
  const [state, setState] = useState({});

  const setUser = (user) => {
    setState({ ...state, user });
  };

  const unsetUser = () => {
    setState({ ...state, user: undefined });
  };

  const ctxValue = {
    state,
    setUser,
    unsetUser,
  };

  return (
    <UserContext.Provider value={ctxValue}>
      {props.children}
    </UserContext.Provider>
  );
};
