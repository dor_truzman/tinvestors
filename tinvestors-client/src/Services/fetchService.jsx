const SERVER_URL = "http://127.0.0.1:3000";
const AI_URL = "http://127.0.0.1:3003";

const fetchToken = () => localStorage.getItem("token");

const createEntity = async ({ name, entity, useAi }) => {
  const result = await fetch(`${useAi ? AI_URL : SERVER_URL}/${name}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${fetchToken()}`,
    },
    body: JSON.stringify(entity),
  });

  return result.ok;
};

const updateEntityById = async ({ name, id, entity, useAi }) => {
  const result = await fetch(`${useAi ? AI_URL : SERVER_URL}/${name}/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${fetchToken()}`,
    },
    body: JSON.stringify(entity),
  });

  return result.ok;
};

const deleteEntityById = async ({ name, id, useAi }) => {
  const result = await fetch(`${useAi ? AI_URL : SERVER_URL}/${name}/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${fetchToken()}`,
    },
  });

  return result.ok;
};

const getEntityById = async ({ name, id, useAi, outputAsText = false }) => {
  const result = await fetch(`${useAi ? AI_URL : SERVER_URL}/${name}/${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${fetchToken()}`,
    },
  });

  if (result.ok) return outputAsText ? result.text() : result.json();
  else throw new Error(`Cannot get ${name}`);
};

const getEntities = async ({ name, useAi, outputAsText = false }) => {
  const result = await fetch(`${useAi ? AI_URL : SERVER_URL}/${name}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${fetchToken()}`,
    },
  });

  if (result.ok) return outputAsText ? result.text() : result.json();
  else throw new Error(`Cannot get ${name}`);
};

const getEntity = async ({ name, route, useAi, outputAsText = false }) => {
  const result = await fetch(
    `${useAi ? AI_URL : SERVER_URL}/${name}${route ? "/" + route : ""}`,
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${fetchToken()}`,
      },
    }
  );

  console.log(fetchToken());
  if (result.ok) return outputAsText ? result.text() : result.json();
  else throw new Error(`Cannot post to ${name}s`);
};

const postEntity = async ({
  name,
  entity,
  route,
  useAi,
  useFormData,
  outputAsText,
}) => {
  const result = await fetch(
    `${useAi ? AI_URL : SERVER_URL}/${name}${route ? "/" + route : ""}`,
    {
      method: "POST",
      headers: useFormData
        ? { Authorization: `Bearer ${fetchToken()}` }
        : {
            "Content-Type": "application/json",
            Authorization: `Bearer ${fetchToken()}`,
          },
      body: useFormData ? getFormData(entity) : JSON.stringify(entity),
    }
  );

  if (result.ok) return outputAsText ? result.text() : result.json();
  else throw new Error(`Cannot post to ${name}`);
};

const getFormData = (object) => {
  const formData = new FormData();
  Object.keys(object).forEach((key) => formData.append(key, object[key]));
  return formData;
};

export {
  SERVER_URL,
  AI_URL,
  createEntity,
  updateEntityById,
  deleteEntityById,
  getEntityById,
  getEntities,
  getEntity,
  postEntity,
};
