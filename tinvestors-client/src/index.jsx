import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import theme from "./MUI/theme";
import { ThemeProvider } from "@emotion/react";
import { BrowserRouter as Router } from "react-router-dom";
import { UserContextProvider } from "./Contexts/userContext";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <LocalizationProvider dateAdapter={AdapterMoment}>
        <UserContextProvider>
          <Router>
            <App />
          </Router>
        </UserContextProvider>
      </LocalizationProvider>
    </ThemeProvider>
  </React.StrictMode>
);
