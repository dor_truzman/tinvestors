import ChatRoom from "../Components/ChatRoom";
import ChatRoomList from "../Components/ChatRoomList";
import {UserContext} from "../Contexts/userContext";
import {getEntities} from "../Services/fetchService";
import {useContext, useEffect, useState} from "react";

function Chat() {
    const userState = useContext(UserContext);
    const [chatListRooms, setChatListRooms] = useState([]);
    const [chatList, setChatList] = useState([]);
    const [loading, setLoading] = useState(false);
    const userId =  userState?.state?.user?._id;
    const [currRoomId, setCurrRoomId] = useState();

    const fetchChatList = async () => {
        try {
            setLoading(true);

            const rooms = await getEntities({
                name: `messages/rooms/userid/${userId}`,
            });

            let chats = [];
            await Promise.all(rooms.map(async (currRoom) => {
                chats.push(await getEntities({
                    name: `messages/room/${currRoom}`
                }));
            }));

            setChatList(chats);
            setLoading(false);
            //
            //       .then((response) => {
            //                  // setChatList([...chatList, response])
            //                  setChatList([...chatList, response]);
            //                  chatList.map((x) => console.log("2: " + x));
            //              }
            //              // setChatList([...chatList, response]);
            //          ).then((res) => setLoading(false));
            //      }
            // ))
            // );

        } catch (e) {
            console.log("Error fetching projects: ", e);
            setLoading(false);
        }
    };

    useEffect(() => {
        if (userId) {
            fetchChatList();
        }
    }, [userId]);

    return (
      <div style={{display: "flex"}}>
        <ChatRoomList chatList={chatList} loading={loading} setCurrRoomId={setCurrRoomId}/>
        <ChatRoom currChat={chatList.find((currChat) => currChat[0].chat_room_name == currRoomId)} currRoomId={currRoomId}/>
      </div>
  );
}

export default Chat;
