import Post from "../Components/Post";
import { Button, Card, CircularProgress, Typography } from "@mui/material";
import { UserContext } from "../Contexts/userContext";
import { Fragment, useContext, useEffect, useState } from "react";
import { getEntities, postEntity } from "../Services/fetchService";
import Sidebar from "../Components/Sidebar";
import { PROJECT_CATEGORIES } from "../Constants/projectCategories";
import Lottie from "lottie-react";
import notFoundAnimation from "../assets/14171-empty.json";
import nothingSearchedAnimation from "../assets/90988-no-results.json";
import { ScrollMenu } from "react-horizontal-scrolling-menu";
import "react-horizontal-scrolling-menu/dist/styles.css";
import "./hideScrollbar.css";
import { LeftArrow, RightArrow } from "../Components/Arrows";
import FilterIcon from "@mui/icons-material/FilterAlt";

function onWheel(apiObj, ev) {
  const isTouchpad = Math.abs(ev.deltaX) !== 0 || Math.abs(ev.deltaY) < 15;

  if (isTouchpad) {
    ev.stopPropagation();
    return;
  }

  if (ev.deltaY < 0) {
    apiObj.scrollNext();
  } else if (ev.deltaY > 0) {
    apiObj.scrollPrev();
  }
}

function Browser() {
  const [trends, setTrends] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const userState = useContext(UserContext);
  const [selectedCategories, setSelectedCategories] = useState(
    PROJECT_CATEGORIES.reduce((obj, item) => {
      return {
        ...obj,
        [item]: false,
      };
    }, {})
  );

  const [categoryPosts, setCategoryPosts] = useState(
    PROJECT_CATEGORIES.reduce((obj, item) => {
      return {
        ...obj,
        [item]: null,
      };
    }, {})
  );

  useEffect(() => {
    fetchTrends();
  }, []);

  useEffect(() => {
    setLoading(true);
  }, [selectedCategories]);

  useEffect(() => {
    if (!!loading) fetchPosts();
  }, [loading]);

  const fetchTrends = async () => {
    try {
      let trends = await getEntities({
        name: "trends",
        useAi: true,
      });
      setTrends(trends);
    } catch (e) {
      console.log("Error fetching trends: ", e);
    }
  };

  const fetchPosts = async () => {
    try {
      const newCategoryPosts = {};

      await Promise.all(
        Object.keys(selectedCategories).map(async (category) => {
          if (!selectedCategories[category]) {
            newCategoryPosts[category] = null;
          } else if (categoryPosts[category]) {
            newCategoryPosts[category] = categoryPosts[category];
          } else {
            newCategoryPosts[category] = await getEntities({
              name: `projects/byCategory/${category}`,
            });
          }
        })
      );

      setCategoryPosts(newCategoryPosts);
    } catch (e) {
      console.log("Error fetching posts: ", e);
      setLoading(false);
    }

    setLoading(false);
  };

  const setFinishedWatching = async ({ postId, userId, timeWatched }) => {
    let normalizedSeconds = Math.round(timeWatched / 1000);
    if (normalizedSeconds < 1) normalizedSeconds = 1;
    else if (normalizedSeconds > 15) normalizedSeconds = 15;

    try {
      await postEntity({
        name: "update_model",
        entity: {
          user_id: userId,
          item_id: postId,
          screen_time: normalizedSeconds,
        },
        useAi: true,
        useFormData: true,
        outputAsText: true,
      });
    } catch {}
  };

  const currCategoriesToShow = PROJECT_CATEGORIES.filter(
    (category) => selectedCategories[category]
  ).toReversed();

  const isAnyCategorySelected = PROJECT_CATEGORIES.find(
    (category) => selectedCategories[category]
  );

  return (
    <>
      <Sidebar
        isSidebarOpen={isSidebarOpen}
        setIsSidebarOpen={setIsSidebarOpen}
        setSelectedCategories={setSelectedCategories}
      />
      <Button
        size={"large"}
        color="info"
        variant="contained"
        style={{ fontSize: 50, marginLeft: 10 }}
        onClick={() => setIsSidebarOpen(true)}
      >
        <FilterIcon />
      </Button>
      <div
        style={{
          width: "50%",
          marginRight: "auto",
          marginLeft: isSidebarOpen ? 610 : "auto",
          marginTop: "auto",
          marginBottom: "auto",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            alignSelf: "center",
            justifyItems: "center",
            textAlign: "center",
          }}
        >
          <Card
            raised
            style={{
              padding: 10,
              marginBottom: 50,
              width: "70%",
              height: 70,
              borderWidth: 5,
              borderRadius: 20,
            }}
          >
            <Typography fontSize={20} fontWeight={"bold"}>
              🌐 CURRENT WORLD TRENDS 🌐
            </Typography>
            <marquee behavior="scroll" direction="left">
              <Typography fontStyle={"italic"} fontFamily={"Courier"}>
                {trends?.length ? trends.join(", ") : "..."}
              </Typography>
            </marquee>
          </Card>

          {!isAnyCategorySelected ? (
            <div
              style={{
                height: 200,
                width: 1000,
                display: "flex",
                justifyContent: "center",
                justifyItems: "center",
                alignContent: "center",
                alignItems: "center",
              }}
            >
              <Typography color="white" variant="h5">
                No categories selected yet. Select any category to show it here.
              </Typography>
              <Lottie
                animationData={nothingSearchedAnimation}
                loop
                style={{ height: 200 }}
              />
            </div>
          ) : (
            currCategoriesToShow.map((category) => (
              <Fragment key={`postsOf-${category}`}>
                <div style={{ alignSelf: "center" }}>
                  <Typography variant="h5" color="white">
                    {category.toUpperCase()}
                  </Typography>
                </div>
                {!loading &&
                categoryPosts[category] &&
                !categoryPosts[category]?.length ? (
                  <div
                    style={{
                      height: 200,
                      width: 1000,
                      display: "flex",
                      justifyContent: "center",
                      justifyItems: "center",
                      alignContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Typography fontStyle="italic" color="white">
                      Nothing to see here...
                    </Typography>
                    <Lottie
                      animationData={notFoundAnimation}
                      loop
                      style={{ height: 200 }}
                    />
                  </div>
                ) : loading && !categoryPosts[category] ? (
                  <div
                    style={{
                      height: 430,
                      width: 1000,
                      display: "flex",
                      justifyContent: "center",
                      justifyItems: "center",
                      alignContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <CircularProgress color="secondary" size={120} />
                  </div>
                ) : (
                  <div
                    style={{
                      width: "85vw",
                      minHeight: 430,
                    }}
                  >
                    <ScrollMenu
                      RightArrow={<RightArrow />}
                      LeftArrow={<LeftArrow />}
                      onWheel={onWheel}
                    >
                      {categoryPosts[category]?.map((post) => (
                        <Post
                          key={post._id}
                          itemId={post._id}
                          current_username={userState?.state?.user?._id}
                          setFinishedWatching={setFinishedWatching}
                          {...post}
                        />
                      ))}
                    </ScrollMenu>
                  </div>
                )}
              </Fragment>
            ))
          )}
        </div>
      </div>
    </>
  );
}

export default Browser;
