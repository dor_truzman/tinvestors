import CloseIcon from "@mui/icons-material/Close";
import {
  Alert,
  Button,
  Card,
  CardContent,
  FormControl,
  IconButton,
  Input,
  Modal,
  Snackbar,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { postEntity } from "../Services/fetchService";
import { useContext, useEffect, useRef, useState } from "react";
import { UserContext } from "../Contexts/userContext";
import isEmail from "is-email";
import {
  useRive,
  useStateMachineInput,
  Layout,
  Fit,
  Alignment,
} from "@rive-app/react-canvas";
import loginTeddy from "../assets/login-teddy.riv";
const STATE_MACHINE_NAME = "Login Machine";

function Login({ isOpen, setIsOpen }) {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [alertObject, setAlertObject] = useState({});
  const [open, setOpen] = useState(false);
  const userState = useContext(UserContext);
  const [inputLookMultiplier, setInputLookMultiplier] = useState(0);
  const { rive, RiveComponent } = useRive({
    src: loginTeddy,
    stateMachines: STATE_MACHINE_NAME,
    autoplay: true,
    layout: new Layout({
      fit: Fit.FitHeight,
      alignment: Alignment.Center,
    }),
  });
  const inputRef = useRef(null);

  useEffect(() => {
    if (inputRef?.current && !inputLookMultiplier) {
      setInputLookMultiplier(inputRef.current.offsetWidth / 100);
    }
  }, [inputRef]);

  const isCheckingInput = useStateMachineInput(
    rive,
    STATE_MACHINE_NAME,
    "isChecking"
  );
  const numLookInput = useStateMachineInput(
    rive,
    STATE_MACHINE_NAME,
    "numLook"
  );
  const trigSuccessInput = useStateMachineInput(
    rive,
    STATE_MACHINE_NAME,
    "trigSuccess"
  );
  const trigFailInput = useStateMachineInput(
    rive,
    STATE_MACHINE_NAME,
    "trigFail"
  );
  const isHandsUpInput = useStateMachineInput(
    rive,
    STATE_MACHINE_NAME,
    "isHandsUp"
  );

  const onEmailChange = (e) => {
    const newVal = e.target.value;
    setEmail(newVal);
    if (isCheckingInput && !isCheckingInput.value) {
      isCheckingInput.value = true;
    }

    const numChars = newVal.length;
    if (numLookInput) {
      numLookInput.value = numChars * inputLookMultiplier;
    }
  };

  const onUsernameFocus = () => {
    if (isCheckingInput) {
      isCheckingInput.value = true;
    }

    if (
      numLookInput &&
      numLookInput.value !== email.length * inputLookMultiplier
    ) {
      numLookInput.value = email.length * inputLookMultiplier;
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const loginFunction = async (event) => {
    event.preventDefault();
    if (isCheckingInput) isCheckingInput.value = false;
    let foundUser = false;

    try {
      foundUser = await postEntity({
        name: "auth",
        entity: { email, password },
        route: "signin",
      });
    } catch (e) {
      if (trigFailInput) trigFailInput.fire();
      setAlertObject({
        type: "warning",
        msg: "Sorry, we cannot find that email and password combination.",
      });
      setOpen(true);
    }

    if (foundUser?.user) {
      if (!foundUser.user.is_approved) {
        if (trigFailInput) trigFailInput.fire();
        setAlertObject({
          type: "warning",
          msg: "We have not yet verified your user. Try again at a later time.",
        });
        setOpen(true);
      } else {
        if (trigSuccessInput) trigSuccessInput.fire();
        setAlertObject({
          type: "success",
          msg: "Great success! Logging you in...",
        });
        setOpen(true);
        setTimeout(() => {
          localStorage.setItem("token", foundUser.jwt);
          userState.setUser({ ...foundUser.user, password: undefined });
          navigate("/");
          setIsOpen(false);
        }, 2000);
      }
    } else {
      if (trigFailInput) trigFailInput.fire();
      setAlertObject({
        type: "warning",
        msg: "Sorry, we cannot find that email and password combination.",
      });
      setOpen(true);
    }
  };

  return (
    <Modal
      keepMounted
      maxWidth="md"
      width="90%"
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
      open={isOpen}
      onClose={() => setIsOpen(false)}
    >
      <div
        style={{
          width: "100%",
          outline: "none",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            justifyItems: "center",
            justifySelf: "center",
            alignSelf: "center",
            alignItems: "center",
            alignContent: "center",
            textAlign: "center",
          }}
        >
          <Card raised sx={{ borderRadius: 30, width: "30%" }}>
            <div
              style={{
                background: "#d7e2ea",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                justifyItems: "center",
                justifySelf: "center",
                alignSelf: "center",
                alignItems: "center",
              }}
            >
              <IconButton
                style={{
                  marginRight: "17%",
                  alignSelf: "flex-end",
                }}
                onClick={() => setIsOpen(false)}
              >
                <CloseIcon style={{ color: "black" }} />
              </IconButton>
              {isOpen && <RiveComponent style={{ width: 300, height: 250 }} />}
            </div>
            <CardContent>
              <FormControl style={{ width: "50%" }}>
                <Input
                  inputRef={inputRef}
                  style={{ fontSize: 25 }}
                  placeholder="Email"
                  id="email"
                  value={email}
                  onFocus={onUsernameFocus}
                  onBlur={() => {
                    if (isCheckingInput) isCheckingInput.value = false;
                  }}
                  onChange={onEmailChange}
                />
                <Input
                  style={{ fontSize: 25 }}
                  placeholder="Password"
                  id="password"
                  type="password"
                  value={password}
                  onFocus={() => {
                    if (isHandsUpInput) isHandsUpInput.value = true;
                  }}
                  onBlur={() => {
                    if (isHandsUpInput) isHandsUpInput.value = false;
                  }}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <Button
                  disabled={
                    !email.length || !password.length || !isEmail(email)
                  }
                  onClick={(e) => loginFunction(e)}
                  style={{ margin: 20, fontSize: 20, fontWeight: "bold" }}
                  variant="contained"
                  color="primary"
                >
                  Sign In
                </Button>
                <Button
                  onClick={() => {
                    navigate("/register");
                    setIsOpen(false);
                  }}
                  style={{ fontSize: 12 }}
                  color="secondary"
                >
                  New User? Register Here
                </Button>
              </FormControl>
            </CardContent>
          </Card>

          <Snackbar
            anchorOrigin={{ horizontal: "center", vertical: "bottom" }}
            open={open}
            autoHideDuration={10000}
            onClose={handleClose}
          >
            <Alert
              variant="filled"
              onClose={handleClose}
              severity={alertObject.type ? alertObject.type : "success"}
              sx={{ width: "100%" }}
            >
              {alertObject.msg}
            </Alert>
          </Snackbar>
        </div>
      </div>
    </Modal>
  );
}

export default Login;
