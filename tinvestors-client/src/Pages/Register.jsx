import { Card, CardContent, Typography } from "@mui/material";
import { useState } from "react";
import IdentificationForm from "../Components/IdentificationForm";
import RegistrationForm from "../Components/RegistrationForm";
import Lottie from "lottie-react";
import pendingAnimation from "../assets/99272-pending-confirmation.json";
import welcomeAnimation from "../assets/72342-welcome.json";
import { postEntity } from "../Services/fetchService";
import { uploadAndGetURL } from "../Utils/firebase";

function Register() {
  const [isOpen, setIsOpen] = useState(false);
  const [isValidated, setIsValidated] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false);
  const [validationDetails, setValidationDetails] = useState({});

  const register = async (userData) => {
    userData.photo_url = await uploadAndGetURL(userData.photo_url);
    await postEntity({ name: "auth", route: "signup", entity: userData });
    setIsRegistered(true);
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
      }}
    >
      <IdentificationForm
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        setIsValidated={setIsValidated}
        validationDetails={validationDetails}
        setValidationDetails={setValidationDetails}
      />

      <Card raised sx={{ borderRadius: 30, width: "55%", marginTop: 2 }}>
        <div
          style={{
            background: "white",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            justifyItems: "center",
            justifySelf: "center",
            alignSelf: "center",
            alignItems: "center",
          }}
        >
          <Lottie
            animationData={welcomeAnimation}
            loop
            style={{ height: 300 }}
          />
        </div>

        <CardContent>
          {isRegistered ? (
            <>
              <Typography fontWeight={"bold"} fontSize={25}>
                The registration request has been sent successfully!
              </Typography>
              <Lottie
                animationData={pendingAnimation}
                loop
                style={{ height: 200 }}
              />
              <Typography fontWeight={"bold"}>
                One of our administrators will examine your request as soon as
                possible.
              </Typography>
            </>
          ) : (
            <RegistrationForm
              setIsOpen={setIsOpen}
              isValidated={isValidated}
              validationDetails={validationDetails}
              register={register}
            />
          )}
        </CardContent>
      </Card>
    </div>
  );
}

export default Register;
