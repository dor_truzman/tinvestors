import { useContext, useEffect, useState } from "react";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { ContentState, EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { PROJECT_CATEGORIES } from "../Constants/projectCategories";
import { postEntity, updateEntityById } from "../Services/fetchService";
import { UserContext } from "../Contexts/userContext";
import { uploadAndGetURL } from "../Utils/firebase";
import { useLocation, useNavigate } from "react-router-dom";

function ProjectEditor() {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [projectDetails, setProjectDetails] = useState({
    category: PROJECT_CATEGORIES[0],
    requested_sum: 0,
  });
  const userState = useContext(UserContext);
  const navigate = useNavigate();

  const { state } = useLocation();
  const { projectToEdit } = state || {};

  useEffect(() => {
    setProjectDetails({
      ...projectDetails,
      html: draftToHtml(convertToRaw(editorState.getCurrentContent())),
    });
  }, [editorState]);

  useEffect(() => {
    if (projectToEdit) {
      setProjectDetails({
        ...projectToEdit,
        photo_url: projectToEdit.images?.length && projectToEdit.images[0],
      });

      const contentBlock = htmlToDraft(projectToEdit.html);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        setEditorState(editorState);
      }
    }
  }, [projectToEdit]);

  const submit = async () => {
    try {
      const entrepreneur_id = userState?.state?.user?._id;
      if (entrepreneur_id) {
        let images = [];
        if (
          projectToEdit &&
          projectToEdit.images?.length &&
          projectDetails.photo_url === projectToEdit.images[0]
        ) {
          images = projectToEdit.images;
        } else {
          images.push(await uploadAndGetURL(projectDetails.photo_url));
        }

        if (projectToEdit) {
          await updateEntityById({
            name: "projects",
            id: projectToEdit._id,
            entity: {
              ...projectDetails,
              photo_url: undefined,
              images,
              entrepreneur_id,
            },
          });
        } else {
          await postEntity({
            name: "projects",
            entity: {
              ...projectDetails,
              images,
              entrepreneur_id,
            },
          });
        }

        navigate("/profile");
      } else throw new Error("Error getting entrepreneur id");
    } catch (e) {
      console.log("Error posting new project", e);
    }
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        justifyItems: "center",
        alignContent: "center",
        alignItems: "center",
      }}
    >
      <Typography color="white" variant="h3">
        {projectToEdit
          ? "Edit Existing Project"
          : "Welcome, Visionary Entrepreneurs!"}
      </Typography>
      <Box style={{ width: "50%", margin: 20 }}>
        <Typography color="white" style={{ fontSize: 15 }}>
          This is your platform to shine, turning dreams into reality.
          <br />
          Share the essence of your project, its unique value proposition, and
          the untapped potential waiting to be unleashed. Craft your story,
          ignite curiosity, and inspire investors to join you on this journey.
          <br />
          Take this opportunity to make a lasting impression and attract the
          support you need to propel your vision forward.
        </Typography>
        <Typography color="white" fontWeight={"bold"}>
          The stage is set, and the spotlight is on you. Show the world what
          sets your project apart, and let the investors witness the incredible
          possibilities that lie ahead.
        </Typography>
      </Box>

      <FormControl style={{ width: "50%", marginBottom: 20 }}>
        <FormLabel style={{ fontWeight: "bold" }}>{"Project Title:"}</FormLabel>
        <TextField
          style={{ fontSize: 25, marginTop: 20 }}
          placeholder="Title (Up to 60 characters)"
          inputProps={{ maxLength: 60 }}
          value={projectDetails.title}
          onChange={(e) =>
            setProjectDetails({ ...projectDetails, title: e.target.value })
          }
        />
      </FormControl>
      <FormControl style={{ width: "50%", marginBottom: 20 }}>
        <FormLabel style={{ fontWeight: "bold" }}>
          {"Project Short Description:"}
        </FormLabel>
        <TextField
          style={{ fontSize: 25, marginTop: 20 }}
          placeholder="Description (Up to 100 characters)"
          inputProps={{ maxLength: 100 }}
          value={projectDetails.description}
          onChange={(e) =>
            setProjectDetails({
              ...projectDetails,
              description: e.target.value,
            })
          }
        />
      </FormControl>
      <FormControl style={{ width: "50%", marginBottom: 20 }}>
        <FormLabel style={{ fontWeight: "bold" }}>
          {"Requested Sum (In $):"}
        </FormLabel>
        <TextField
          type="number"
          InputProps={{ inputProps: { min: 0 } }}
          style={{ fontSize: 25, width: "20%", marginTop: 20 }}
          placeholder="Sum"
          value={projectDetails.requested_sum}
          onChange={(e) =>
            setProjectDetails({
              ...projectDetails,
              requested_sum: parseInt(e.target.value),
            })
          }
        />
      </FormControl>
      <FormControl style={{ width: "50%", marginBottom: 20 }}>
        <FormLabel style={{ fontWeight: "bold" }}>
          {"Project Category:"}
        </FormLabel>
        <Select
          style={{ marginTop: 20 }}
          value={projectDetails.category}
          onChange={(e) =>
            setProjectDetails({ ...projectDetails, category: e.target.value })
          }
        >
          {PROJECT_CATEGORIES &&
            PROJECT_CATEGORIES.map((type, idx) => (
              <MenuItem value={type} key={`categoryMenuItem${idx}`}>
                {type}
              </MenuItem>
            ))}
        </Select>
      </FormControl>
      <FormControl style={{ width: "50%", marginBottom: 20 }}>
        <FormLabel style={{ fontWeight: "bold" }}>{"Thumbnail:"}</FormLabel>
        <Button
          component="label"
          color={projectDetails.photo_url ? "primary" : "secondary"}
        >
          {projectDetails.photo_url ? "Saved ✓" : "Upload"}
          <input
            accept="image/*"
            type="file"
            hidden
            onChange={(e) => {
              if (e.target?.files?.length) {
                setProjectDetails({
                  ...projectDetails,
                  photo_url: e.target.files[0],
                });
              }
            }}
          />
        </Button>
      </FormControl>
      <div style={{ width: "50%", marginBottom: 20 }}>
        <FormLabel style={{ fontWeight: "bold" }}>
          {"Project Long Description:"}
        </FormLabel>
        <div style={{ backgroundColor: "white", marginTop: 20 }}>
          <Editor
            editorState={editorState}
            onEditorStateChange={setEditorState}
          />
        </div>
      </div>
      <Button
        style={{ fontSize: 20 }}
        color="secondary"
        disabled={
          !projectDetails.title?.length ||
          !projectDetails.description?.length ||
          !projectDetails.category?.length
        }
        onClick={() => submit()}
      >
        SUBMIT
      </Button>
    </div>
  );
}

export default ProjectEditor;
