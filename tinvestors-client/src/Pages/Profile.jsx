import {
  Box,
  Card,
  Divider,
  Fab,
  Grid,
  Tab,
  Tabs,
  Tooltip,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import ProfileCard from "../Components/ProfileCard";
import { ENTRE_TYPE } from "../Constants/userTypes";
import { useContext, useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import { UserContext } from "../Contexts/userContext";
import MyProjects from "../Components/MyProjects";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <>{children}</>}
    </div>
  );
}

function Profile() {
  const [currTab, setCurrTab] = useState(0);
  const navigate = useNavigate();
  const userState = useContext(UserContext);
  const userInfo = userState?.state?.user || {};

  return (
    <>
      {userInfo?.user_type === ENTRE_TYPE && (
        <Tooltip placement="top" title="Publish a new project">
          <Fab
            onClick={() => navigate("/project-editor")}
            size="large"
            color="primary"
            aria-label="add"
            style={{
              position: "fixed",
              bottom: 50,
              right: 50,
            }}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
      )}

      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Card raised sx={{ width: 800, mx: 2, my: 2 }}>
          <Box sx={{ my: 3, mx: 2, height: 50 }}>
            <Grid container alignItems="center">
              <Grid item xs>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{ fontWeight: "bold" }}
                  component="div"
                >
                  {userInfo.name}
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <Typography
                gutterBottom
                fontSize={18}
                fontStyle="italic"
                component="div"
              >
                {userInfo.user_type}
              </Typography>
            </Grid>
          </Box>
          <Divider variant="middle" sx={{ marginTop: 5 }} />
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              justifyItems: "center",
              alignItems: "center",
              textAlign: "center",
              alignSelf: "center",
              alignContent: "center",
              marginBottom: 10,
            }}
          >
            <Tabs value={currTab} onChange={(e, v) => setCurrTab(v)}>
              <Tab label="Information" {...a11yProps(0)} />
              {userInfo?.user_type === ENTRE_TYPE && (
                <Tab label="Projects" {...a11yProps(1)} />
              )}
            </Tabs>
          </div>

          <TabPanel
            value={currTab}
            index={0}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              justifyItems: "center",
              alignItems: "center",
              alignSelf: "center",
              alignContent: "center",
              textAlign: "center",
            }}
          >
            <ProfileCard {...userInfo} favorite_categories={["AI, FinTech"]} />
          </TabPanel>
          <TabPanel value={currTab} index={1}>
            <MyProjects userId={userInfo?._id} />
          </TabPanel>
        </Card>
      </div>
    </>
  );
}

export default Profile;
