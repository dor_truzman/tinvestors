import ThumbUp from "@mui/icons-material/ThumbUp";
import ThumbUpOff from "@mui/icons-material/ThumbUpOffAlt";
import Chat from "@mui/icons-material/ChatSharp";
import {
  Box,
  Card,
  Divider,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { postEntity } from "../Services/fetchService";
import { useLocation, useNavigate } from "react-router-dom";

function PostDisplay({ route }) {
  const [likeStatus, setLike] = useState(false);
  const { state } = useLocation();
  const navigate = useNavigate();

  const {
    _id,
    title,
    description,
    entrepreneur_id,
    liked_by,
    current_username,
    requested_sum,
    images,
    category,
    matchPercent,
    html,
  } = state || {};

  useEffect(() => {
    setLike(
      !!(
        liked_by?.length &&
        liked_by?.find((user) => user._id === current_username)
      )
    );
  }, [_id]);

  const setLikeStatus = async (newStatus) => {
    await postEntity({
      name: "projects",
      entity: {
        projectId: _id,
        investorId: current_username,
      },
      route: "like",
    });

    setLike(newStatus);
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
      }}
    >
      <Card raised sx={{ width: "50%", mx: 2, my: 7, borderRadius: 20 }}>
        <Box
          sx={{
            my: 3,
            mx: 2,
            height: 80,
          }}
        >
          <Grid container alignItems="center" style={{ height: 70 }}>
            <Grid item xs>
              <Typography
                gutterBottom
                fontSize={30}
                style={{ fontWeight: "bold" }}
                component="div"
              >
                {title}
              </Typography>
            </Grid>
          </Grid>
          <Grid item style={{ height: 10 }}>
            <Typography
              gutterBottom
              fontSize={20}
              fontStyle="italic"
              component="div"
            >
              {"Category: " +
                category +
                ", Entrepreneur: " +
                (entrepreneur_id?.name || "")}
            </Typography>
          </Grid>
        </Box>
        <Divider variant="middle" />
        {images?.length ? (
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-around"
            flexDirection={"row"}
            height={120}
            margin={5}
          >
            <img
              src={images[0]}
              style={{
                maxWidth: 150,
                maxHeight: 150,
                margin: 10,
                borderRadius: "20%",
              }}
              alt={"Thumbnail"}
            />
          </Box>
        ) : (
          <></>
        )}
        <Box
          sx={{ my: 2, mx: 2 }}
          display="flex"
          alignItems="center"
          justifyContent="center"
          flexDirection={"column"}
          height={70}
        >
          <Typography style={{ marginBottom: 10 }} fontWeight={"bold"}>
            Project Short Description:
          </Typography>
          <Typography>{description}</Typography>
        </Box>
        {html?.length && (
          <Box style={{ marginTop: 40 }}>
            <Typography fontWeight={"bold"}>Project Details:</Typography>
            <Card
              raised
              style={{ margin: 20 }}
              dangerouslySetInnerHTML={{ __html: html }}
            />
          </Box>
        )}

        <Typography style={{ fontSize: 20 }}>Requested sum:</Typography>
        <Typography style={{ fontSize: 25 }} fontWeight={"bold"}>
          {requested_sum.toLocaleString("en-US")} $
        </Typography>
        <Box style={{ margin: 20 }}>
          <Typography fontStyle={"italic"}>
            {likeStatus && "You and "} {liked_by.length}{" "}
            {likeStatus && " other"} investors liked this project
          </Typography>
        </Box>
        <Box display="flex" justifyContent="center" alignItems="center">
          <IconButton
            size="large"
            color="primary"
            aria-label="upload picture"
            component="label"
            onClick={() => setLikeStatus(!likeStatus)}
          >
            {likeStatus ? (
              <ThumbUp style={{ fontSize: 50 }} />
            ) : (
              <ThumbUpOff style={{ fontSize: 50 }} />
            )}
          </IconButton>
          <IconButton
            size="large"
            color="success"
            aria-label="upload picture"
            component="label"
            onClick={() =>
              navigate("/chat", {
                state: {
                  _id,
                  title,
                  description,
                  entrepreneur_id,
                  liked_by,
                  current_username,
                  requested_sum,
                  images,
                  category,
                  matchPercent,
                  html,
                },
              })
            }
          >
            <Chat style={{ fontSize: 50 }} />
          </IconButton>
        </Box>
      </Card>
    </div>
  );
}

export default PostDisplay;
