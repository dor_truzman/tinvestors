import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import StockMarket from "../assets/stockmarket.jpg";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Lottie from "lottie-react";
import handShackAnimation from "../assets/27773-hand-shake-business-deal.json";

function Exposition() {
  const navigate = useNavigate();

  return (
    <div
      style={{
        // height: "100vh",
        // backgroundImage: `url(${StockMarket}`,
        backgroundSize: "cover",
        display: "flex",
        textAlign: "center",
        flexDirection: "column",
        alignContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          textAlign: "center",
          flexDirection: "column",
          alignContent: "center",
          alignItems: "center",
          padding: 50,
        }}
      >
        <Typography
          variant="h3"
          fontWeight={"bold"}
          color={"white"}
          style={{ margin: 10, width: "60%" }}
        >
          Connecting investors and entrepreneurs around the world. Safely.
        </Typography>
        <Typography
          variant="h5"
          color={"white"}
          style={{
            margin: 10,
            padding: 5,
          }}
        >
          Join us now in our quest for sustainable business relationships.
        </Typography>
        <Button
          size="large"
          style={{ fontSize: 25, margin: 10 }}
          variant="contained"
          onClick={() => navigate("/register")}
        >
          JOIN THE FAMILY
        </Button>
      </div>

      <Lottie
        animationData={handShackAnimation}
        loop
        style={{ height: 500, marginRight: "60%", position: "absolute" }}
      />

      <Typography
        variant="h5"
        color={"white"}
        style={{ marginTop: 110, marginBottom: 35 }}
      >
        Frequently Asked Questions
      </Typography>
      <Accordion style={{ width: "55%" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>What is Tinvestors?</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Tinvestors is a cutting-edge platform that serves as a bridge
            between entrepreneurs and investors. It is designed to connect these
            two vital stakeholders in the business world and facilitate
            meaningful collaborations.
            <br />
            At its core, Tinvestors is an online marketplace where entrepreneurs
            can showcase their innovative project ideas and investors can
            discover promising ventures to support financially. We understand
            that finding the right investor or the perfect project can be a
            daunting task, and that's where Tinvestors comes in.
            <br />
            Our platform utilizes data analysis to match entrepreneurs with
            investors based on their shared interests, project ideas, and other
            relevant parameters. By leveraging this intelligent matching system,
            Tinvestors ensures that entrepreneurs connect with investors who
            have a genuine interest in their specific industries and concepts.
            <br />
            Whether you are an entrepreneur with an innovative idea seeking
            funding or an investor looking for exciting investment
            opportunities, Tinvestors provides a user-friendly and efficient
            platform to meet your needs.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "55%" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>
            How can I know that the other party is legitimate?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            At Tinvestors, we prioritize legitimacy and trust. Here's how we
            ensure the other party is legitimate:
            <br />
            1. Verified Profiles: Users complete detailed profiles, providing
            accurate information.
            <br />
            2. Physical Identification: We require users to submit a picture of
            their ID for verification.
            <br />
            Trust Tinvestors to create a secure platform for authentic
            connections between entrepreneurs and investors.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "55%" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>
            What measures are you taking to keep my data secure?
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Here's what we do to keep your data safe:
            <br />
            1. Data Encryption: We use industry-standard encryption to protect
            your information.
            <br />
            2. Secure Infrastructure: Our platform is built on a secure
            infrastructure with strong security measures.
            <br />
            3. User Authentication: We employ secure authentication methods to
            verify user identity.
            <br />
            4. Limited Access: Access to your data is restricted to authorized
            personnel only.
          </Typography>
        </AccordionDetails>
      </Accordion>

      <div
        style={{
          display: "flex",
          textAlign: "center",
          flexDirection: "column",
          alignContent: "center",
          alignItems: "center",
          marginTop: 100,
        }}
      >
        <Typography color="white">Tinvestors Israel ©</Typography>
      </div>
    </div>
  );
}

export default Exposition;
