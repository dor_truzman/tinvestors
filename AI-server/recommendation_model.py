import pandas as pd
import time
from surprise import Dataset, Reader, SVD
from surprise.model_selection import cross_validate

data_set = './investment_data.xlsx'
RATING_CONVERTER = 20
model = SVD()
lock = False


def init_model():
    global lock
    lock = True
    df = pd.read_excel(data_set, engine='openpyxl')
    reader = Reader(rating_scale=(1, 5))
    initial_data = Dataset.load_from_df(df[['userID', 'itemID', 'rating']], reader=reader)

    model.fit(initial_data.build_full_trainset())
    metrics = ['rmse', 'mae']
    cv_results = cross_validate(model, initial_data, measures=metrics, cv=5, verbose=True)
    print('Mean RMSE:', cv_results['test_rmse'].mean())
    print('Mean MAE:', cv_results['test_mae'].mean())
    lock = False


def update_model(new_ratings):
    df = pd.read_excel(data_set, engine='openpyxl')
    for new_rating in new_ratings:
        df = df.append(new_rating, ignore_index=True)

    df.to_excel(data_set, index=False)
    init_model()


def predict_rating(user_id, item_id):
    wait_for_unlock()
    rating_prediction = model.predict(user_id, item_id)
    try:
        est_rating_prediction = int(rating_prediction.est.item() * RATING_CONVERTER)
    except:
        est_rating_prediction = 20
    return str(est_rating_prediction)


def wait_for_unlock():
    while lock:
        time.sleep(1)
