import pytrends
from pytrends.request import TrendReq

keyword = "trending"
time_frame = "today 1-m"


def get_trends():
    pytrends = TrendReq(hl='en-US', tz=360)
    pytrends.build_payload(kw_list=[keyword], timeframe=time_frame)
    trending_searches = pytrends.trending_searches()
    return trending_searches.iloc[:10, 0].tolist()
