import recommendation_model
import trends
from flask import Flask, request
from flask_cors import CORS

BATCH_SIZE = 1
MIN = 1
MAX = 15
new_ratings = []

recommendation_model.init_model()
app = Flask(__name__)
CORS(app)


@app.route("/update_model", methods=['POST'])
def update_model():
    user_id = request.form['user_id']
    item_id = request.form['item_id']
    screen_time = request.form['screen_time']
    screen_time = normalize(int(screen_time))
    screen_time = int(screen_time * 5)
    new_rating = {"userID": user_id, "itemID": item_id, "rating": str(screen_time)}
    new_ratings.append(new_rating)

    if len(new_ratings) == BATCH_SIZE:
        recommendation_model.update_model(new_ratings)
        new_ratings.clear()

    return "True"


@app.route("/predict_rating", methods=['POST'])
def predict_rating():
    user_id = request.form['user_id']
    item_id = request.form['item_id']
    return recommendation_model.predict_rating(user_id, item_id)


@app.route("/trends", methods=['GET'])
def get_trends():
    return trends.get_trends()


def normalize(number):
    return (number - MIN) / (MAX - MIN)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3003)
